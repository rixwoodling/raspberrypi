partx

apt install util-linux
man partx
---
# tell the kernel about the presence and numbering of on-disk partitions


partx -s /dev/sdb       # --show human-readable partition list
partx -l /dev/sdb       # --list partitions
partx -u /dev/loop0     # --update kernel partition table

# if error "/dev/loop5p1: read failed after 0 of 4096 at 255918080: Input/output error"
partx -d /dev/loop5p3   # --delete partition

