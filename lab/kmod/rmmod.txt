rmmod

apt install kmod
man rmmod
---

# `uname -r` will use current kernel version 
# /lib/modules/`uname -r`/kernel

rmmod module.ko  #
