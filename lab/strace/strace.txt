strace

apt install strace
strace --help
---

strace ls                         # trace detailed system calls for program
strace -c ls                      # trace system calls summary
strace -C ls                      # detailed and summary of system calls
strace -p 2894                    # monitor system calls for running process



