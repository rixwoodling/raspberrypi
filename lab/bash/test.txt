test

man test
---

# exit codes for these commands equal 0

test 1 == 1             # test 1 equals 1                                
test 1 != 2             # test 1 does not equal 2
test 1 -eq 1            # test 1 equals 1
test 1 -ne 2            # test 1 not equal to 2
test 2 -gt 1            # test 2 greater than 1
test 1 -lt 2            # test 1 less that 2
-ge	Greater than or equal to
-le	Less than or equal to


test -a /               # test if regular file or directory exists
test -b /dev/sda        # test if block file exists
test -c /dev/null       # test if character special file
test -d /               # test if directory exists
test -e /proc/cpuinfo   # test if exists, regardless of type
test -f /proc/cpuinfo   # test if file exists

[ ! -d ./backup ] && mkdir ./backup  # make directory if does not exist

