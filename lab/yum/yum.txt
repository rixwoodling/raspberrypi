yum

apt install yum
yum --help
---

yum update                         # update package list
yum upgrade                        # upgrade packages

yum install bash                   # install package
yum remove bash                    # remove package

yum search bash                    # search package
yum list updates                   # list available updates
yum provides echo                  # show package origin
yum localinstall *.rpm             # install local rpm
yum downgrade bash                 # downgrade package
yum deplist bash                   # list dependancies

yum grouplist                      # list groups
yum groupinfo "Development Tools"  # list tools for development
yum groupinstall "E-mail server"   # install tools for email server
yum groupupdate                    # update groups
yum remove "Security Tools"        # remove tools for Security

yum list "yum-plugin*"             # list plugins for yum
yum install yum-plugin-verify      # install verify package
yum verify bash                    # verify package
yum verify-all                     # verify all packages

yum shell                          # interactive shell


