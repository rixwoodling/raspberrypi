ifconfig

apt install net-tools
man ifconfig
---

ifconfig

Display information about only eth0:

ifconfig eth0

Set the IP address to 192.168.1.50 on interface eth0:

ifconfig eth0 192.168.1.50
ifconfig eth0 netmask 255.255.255.0    # set the netmask to 24-bit
ifconfig eth0 up                       # bring interface eth0 up
ifconfig eth0 down                     # bring interface eth0 down
ifconfig eth0 mtu 1480                 # set the MTU (Maximum Transfer Unit) to 1480 bytes for interface eth0

ifconfig
ifconfig eth0 add 192.168.80.174
ifconfig eth0 del 192.168.80.174
ifconfig eth0 hw ether 00:0c:29:33:4e:aa
ifconfig eth0 mtu 2000
ifconfig eth0 multicast
ifconfig eth0 txqueuelen 1200
ifconfig eth0 promisc
ifconfig eth0 allmulti

ifconfig eth0 arp
ifconfig eth0 -arp

