gdisk

apt install gdisk
man gdisk
---
# partition a hdd bigger than 2 terabytes


gdisk -l /dev/sda      # view disk/partition information


--- CREATE PARTITION

# 1. list block devices
lsblk

# 2. create a new partition
gdisk /dev/sdb
n   # create a new partition
    # enter to accept default creating first partition 
p   # select primary
    # enter to accept the default number of the first sector.
    # enter to accept the default number of the last sector.
    # enter to accept the default hex code = 8300
w   # to write the new partition table.

# 3. confirm partition
lsblk

# 4. reboot
reboot 

# 5. resize partition to fill up entire space
resize2fs /dev/sdb1

# 6. format partition
# see "mkfs.txt"


--- DELETE PARTITION

# 1. delete partition
gdisk /dev/sdb
d   # delete partition
w   # write to confirm

# 2. reboot
reboot

# 3. confirm no partition
lsblk

