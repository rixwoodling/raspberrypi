info

sudo apt install info
man info
---

ls /usr/share/info
info -w cvs 

H           # help with commands
h           # help on using info
l           # leave help menus
ctrl+g      # escape command

down arrow	To move to the next line
up arrow	To move to the previous line
spacebar	To move to the next page
del	        To move to the previous page
]	        To move to the next node
[	        To move to the previous node
t	        To move to the top node of the document
s	        To search a string in the forward direction
{	        To search the previous occurrence of the string
}	        To search the next occurrence of the string
q	        To quit from the document

