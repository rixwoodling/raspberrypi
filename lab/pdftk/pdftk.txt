pdftk

apt install pdftk
man pdftk
---

pdftk 1.pdf 2.pdf cat output 12.pdf                 # merge the two documents 1.pdf and 2.pdf. The output will be saved to 12.pdf
pdftk A=1.pdf cat A1-2 output new.pdf               # write only pages 1 and 2 of 1.pdf. The output will be saved to new.pdf
pdftk A=1.pdf cat A1-endright output new.pdf        # rotate all pages of 1.pdf 90 degrees clockwise and save result in new.pdf
pdftk public.pdf output private.pdf user_pw PROMPT  # password protect pdf file

