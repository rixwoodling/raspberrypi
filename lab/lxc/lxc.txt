lxc

apt install lxc
man lxc
---

sudo apt-get install lxc
ls /usr/share/lxc/templates/
lxc-create -t <template> -n <container name>
lxc-create -t alpine -n mycontainer
sudo lxc-start -n mycontainer
sudo lxc-console -n mycontainer
sudo lxc-stop -n mycontainer


sudo lxc-info -n mycontainer
sudo lxc-attach -n mycontainer
sudo lxc-ls
sudo lxc-destroy -n mycontainer

