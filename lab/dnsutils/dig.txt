dig - DNS lookup utility

apt install dnsutils
---

dig google.com                  # dns lookup
dig google.com +short           # short version (ip output only)
dig google.com +noall +answer   # query all dns record types
dig -x 216.58.193.78 +short     # reverse dns lookup

---

