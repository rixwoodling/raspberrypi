unrar

wget http://www.rarlab.com/rar/unrarsrc-4.2.4.tar.gz
gunzip -v unrarsrc-4.2.4.tar.gz
tar -xvf unrarsrc-4.2.4.tar
cd unrar/
make -f makefile.unix
sudo cp unrar /usr/local/bin
sudo update-alternatives --install /usr/bin/unrar unrar /usr/local/bin/unrar 10
update-alternatives --display unrar
sudo update-alternatives --config unrar
man unrar
---

unrar x *.rar

---
credit: 
james968 raspberrypi.org/forums/viewtopic.php?p=151313
