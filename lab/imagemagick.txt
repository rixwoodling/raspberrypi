imagemagick

sudo apt install imagemagick
man imagemagick
---


convert input.ps output.pdf                       # converts input.ps to output.pdf
convert input.pdf output.ps                       # converts input.pdf to output.ps
convert old.png -crop 640x480+50+100 new.png	  # crop image starting at w50,h100 and output to new
convert old.jpg -resize 300 -quality 75 new.jpg	  # resize image with quality reduction to new file 
identify image.jpg                                # image info such as dimensions, etc.
mogrify	-crop 1920x1080	image.jpg                 # crop file to 1920x1080 pixels
mogrify -path . -resize 1920 image.jpg            # resize an image by width and overwrite


