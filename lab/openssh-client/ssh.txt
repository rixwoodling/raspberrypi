ssh

apt install openssh-client
man ssh
---

/etc/ssh/ssh_config                # outgoing ssh config
/etc/ssh/sshd_config               # incoming ssh config

ssh 192.168.1.100                  # connects as same user
ssh $USER@192.168.1.100            # specifies user or use current
ssh -l $USER 192.168.1.100         # specify user as option
ssh -p 2222 $USER@192.168.1.100    # specifies port
ssh -o Port=2222 user@192.168.1.1  # specifies port (alternative)

