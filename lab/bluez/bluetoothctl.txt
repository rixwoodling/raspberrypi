bluetoothctl

apt install bluez
man bluetoothctl
---

# After configuring bluetooth noted in ../bluetooth.txt,
# connect to Echo Dot speaker with the following steps:
# its important to remove target device and reconnect by
# pairing first after scan, then trust, then connect. 

bluetoothctl
[bluetooth]# devices
[bluetooth]# remove XX:XX:XX:XX:XX:XX
[bluetooth]# scan on
[bluetooth]# scan off
[bluetooth]# pair XX:XX:XX:XX:XX:XX
[bluetooth]# trust XX:XX:XX:XX:XX:XX
[bluetooth]# connect XX:XX:XX:XX:XX:XX
[Echo-92N]# 

[Echo-92N]# help	# display help menu
[Echo-92N]# show	# display blueooth adapter info
[Echo-92N]# info	# display target device info


