stress-ng

apt install stress-ng
man stress-ng
---

stress-ng -c 8          # --cpu 8 process hogs
stress-ng -i 4          # --io 4
stress-ng -m 6          # --vm 6 
stress-ng -c 8 -t 10s   # --timeout 10s

