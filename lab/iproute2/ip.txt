ip

apt install ip
man ip
---

ip addr show                                     # show info for network devices
ip --brief -4 addr show                          # show network IPv4 addresses in brief

ip route show                                    # check route table
ip route list                                    # check route table
ip -s -h link                                    # human readable route table
ip route add default via 192.168.1.1 dev wlp2s0  # add default gateway to device
ip route add 10.5.0.0/16 via 192.168.1.100       # add router gateway to ip
ip route del default                             # remove default gateway temporarily

ip link set eth0 up                              # enable network interface
ip link set eth0 down                            # disable network interface


