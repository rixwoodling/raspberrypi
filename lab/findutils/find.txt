find

apt install findutils
man find
---

find . -name "*.txt"                             # find all txt files within current hierarchy 
find . -name "*.txt" 2>/dev/null                 # find with errors to /dev/null
find . -name "*.txt" -uid 0                      # find files owned by root
find . -name "*.swp" -ok rm {} ’;’               # find and safely remove instances of file
find . -name "*.swp" -exec rm {} ’;’             # find and remove instances of file

find . -perm 777                                 # find all executables
find . -perm 0777                                # 
find . -perm -777                                # 
find . -name "*" -perm /777                      # 
find . -name "*" -perm +4000                     # 
find . -perm /4000                               # find all recursive files with SUID set
find . -perm /2000                               # find all recursive files with SGID set
find . -perm /6000                               # find all recursive files with both SUID and SGID set 

find . -name "*" -size 10k                       # find files 10 kilobytes or more in size
find . -name "*" -size 1M                        # find files 1 megabyte or more in size

find . -type f "*"                               # 
find . -type d "*"                               # 
