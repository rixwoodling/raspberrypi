cmus

---
setup

sudo brew install cmus      # macOS
sudo apt install cmus       # linux

.cmus/autosave              # 
     /cache                 # 
     /command-history       # 
     /lib.pl                # 
     /playlist.pl           # 
     /rc                    # cmus config file
     /search-history        # 
     /socket=               # 


---
commands

: + up arrow                # see command history 
:clear                      # clear library
:update-cache               # update cache
:add /path/Music            # add library
:colorscheme name_of_theme  # place name_of_theme.theme in ./cmus
:colorscheme cyan           # see /usr/share/cmus/ for more themes
:filter genre="Ambient"     # set filter to show genre
:filter                     # clears filter

---
custom keybindings

down    win-down            # move up
up      win-up              # move down
left    win-next            # move left
right   win-next            # move right

space   player-pause        # play/pause
enter   win-activate        # play/restart
        win-toggle          # show album lists
x       player-stop         # stop
,                           # previous track
.                           # next track
<       seek -1m            # skip back 1 minute 
>       seek +1m            # ship forward 1 minute

A       toggle aaa_mode     # all/album/artist mode filter     

C       toggle continue     # 
F       toggle follow       # 
R       toggle repeat       # 
S       toggle shuffle      # 


---
view-specific commands

1       view tree (album artist > album > song lists)
        ---
        i       win-sel-cur         # jump to current track

2       view detailed library (as stacked list) 
        ---
        
3       playlist (playlist view)
        ---
        :clear                      # will clear the playlist
        :save playlist.pls          # will save the current playlist
        :load playlist.pls          # will load playlist.pls

4       play queue 
        ---
        
5       browser view
        ---
        
6       library filters
        ---
        
7       settings
        ---


---
one-time tweaks

:set output_plugin=alsa
:set softvol=true           # enable software volume control
                            # 100% volume matches max volume set by system.
:set softvol=false          # volume set by system. ie: ALSA (alsamixer)
                            # if =false, setting volume with - and + produces
                            # "Error: can't change volume: mixer is not open"
:set format_current= %A %t%= %l %y 
:set format_trackwin= %3n. %t%= %{codec} %y %d 
:set format_trackwin_album= %l %y 
:set format_treewin=  %l  

---
resources

http://www.mdlerch.com/tutorial-to-get-cmus-rockin.html
http://cmus-devel.narkive.com/PMpCYG8E/themes
http://misc.flogisoft.com/bash/tip_colors_and_formatting
https://www.apt-browse.org/browse/ubuntu/trusty/universe/i386/cmus/2.5.0-4build1/file/usr/share/cmus/green.theme

