zgrep

apt install gzip
man zgrep
---

zgrep -i "FOO" file.txt.gz         # case insensitive search
zgrep -n "foo" file.txt.gz         # print line numbers when match found

