enscript

apt install enscript
man enscript
---

enscript -2 -r -p psfile.ps textfile.txt     # convert to 2 columns and rotate to landscape
enscript -p psfile.ps textfile.txt           # convert a text file to PostScript (saved to psfile.ps)
enscript -n -p psfile.ps textfile.txt        # convert a text file to n columns where n=1-9 (saved in psfile.ps)
enscript textfile.txt                        # print a text file directly to the default printer
enscript -p - file.txt  | ps2pdf - file.pdf  # convert directly to pdf with ps2pdf

