sysctl

sudo apt install procps
man sysctl
---

sysctl -a                                               # list all values
sysctl -p                                               # reload /etc/sysctl.conf without reboot

sysctl net.ipv4.icmp_echo_ignore_all                    # print value of icmp_echo_ignore_all
sysctl –w net.ipv4.icmp_echo_ignore_all=1

sysctl kernel.threads-max=100000                        # sh -c 'echo 100000 > /proc/sys/kernel/threads-max'
sysctl net.ipv4.ip_forward=1                            # sh -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'

---
sh -c 'echo net.ipv4.ip_forward=1 >> /etc/sysctl.conf   # for persistant forwarding add to config file


