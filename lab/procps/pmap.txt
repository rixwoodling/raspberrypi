pmap

apt install pmap
pmap --help
---

pmap 1234             # map memory of process id
pmap -d 1234          # show device format       
pmap -px 1234         # show extended path details

