chmod

apt install coreutils
man chmod
---
# change file mode bits

chmod root:root *  # change owner and group for all in dir

chmod 777 *        # -rwxrwxrwx
chmod 755 *        # -rwxr-xr-x 
chmod 644 *        # -rw-r--r-- 
chmod 400 *        # -r--------  

chmod u+x *        # -r-x------
chmod g+rw *       # -r-xrw----
chmod g+x,o+r *    # -rwxrwxr--
chmod ug-wx *      # -r--r--r--
chmod a=rwx *      # -rwxrwxrwx

chmod 4777 *       # -rwsrwxrwx
chmod 0777 *       # -rwxrwxrwx
chmod u+s *        # -rwsrwxrwx
chmod u-s *        # -rwxrwxrwx

chmod 4777 *       # -rwsrwxrwx
chmod 2677 *       # -rwSrwsrwx
chmod 1667 *       # -rwSrwSrwt
chmod -777 *       # ---S--S--T 
chmod +777 *       # -rwsrwsrwt        


# if access directory: Permission denied
cd target && ls -ail target/ && chmod +x target && cd target

