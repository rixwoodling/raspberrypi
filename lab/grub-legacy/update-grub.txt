update-grub

apt install grub-legacy
man update-grub
---

# /etc/grub.d
# /etc/default/grub

update-grub              # reload grub configuration

