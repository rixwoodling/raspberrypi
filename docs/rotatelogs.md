### logs 

---
#### rotate logs

<i>before installing anything from 'apt-get' or accumulating further user history, 
rotate the 'installation' and 'history' logs to separate past build history 
from future user history.</i>

backup dpkg and history logs. 
```
cp /var/log/dpkg.log /var/log/dpkg.log.bak && \
cp /var/log/apt/history.log /var/log/apt/history.log.bak 
```  

empty log files.
```
truncate -s 0 /var/log/dpkg.log && \
truncate -s 0 /var/log/apt/history.log
```

verify logs are empty.
```
grep " install " /var/log/dpkg.log && \
cat /var/log/apt/history.log | grep Commandline
```

---
<sup>`?` http://serverfault.com/questions/175504/how-do-i-get-the-history-of-apt-get-install-on-ubuntu</sup> 

