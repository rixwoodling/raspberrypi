##### shell commands

| bash | tcsh | # | 
| :- | :- | :- |
| `x=1` | `set x=1` | `# local variables are not visible to other programs` |
| `export x=1` | `setenv x 1` | `# environment variables are available to other shell processes` |
| `/etc/profile`<br>`~/.bash_profile`<br>`~/.bash_login`<br>`~/.profile`<br>`~/.bashrc` | `/etc/csh.cshrc`<br>`/etc/csh.login`<br>`~/.tcshrc`<br>`~/.cshrc`<br>`~/.login` | `# order of startup files` | 
| `compgen -b` | `builtins` | `# list shell builtins*` |
| `compgen -e` | | `# list shell variables*` |
><i>both `env` and `printenv` are part of the <b>coreutils</b> package and are not shell dependant.</i>  


