#### Network Static IP 

<sup><i>`eth0` A private static IP address will not change</i></sup>

---

`1` boot into RPi via SSH `ssh root@192.168.X.XXX`

`2` view interfaces file `cat /etc/network/interfaces`

`3` gather network info `ifconfig`
```
eth0  inet addr:192.168.1.102   Bcast:192.168.1.255   Mask:255.255.255.0
```

`4` gather further network info
`netstat -nr` or `route -n`
```
Destination   Gateway
192.168.1.0   192.168.1.1 
```

`5` update interfaces file `sudo nano /etc/network/interfaces`
```
iface eth0 inet dhcp    # remove
```
```
iface eth0 inet static  # append
address 192.168.1.102   # append
netmask 255.255.255.0   # append
broadcast 192.168.1.255 # append
gateway 192.168.1.1     # append
```

`6` remove existing leases `sudo rm /var/lib/dhcp/*`

`7` reboot `sudo reboot`

`8` verify changes `ifconfig`

`9` ping gateway address `ping 192.168.1.1 -c 10`

&nbsp;
---


