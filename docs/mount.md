##### Mount External Hard Drive and Create NFS Server

---
`sudo nfsd status`  
`sudo nfsd start`  
`sudo nfsd restart` #macos  
`sudo nfsd stop` #macos  

---
`dpkg -s nfs-kernel-server`  
`apt-get install nfs-kernel-server`  

`sudo service nfs-kernel-server status` `/etc/init.d/nfs-kernel-server status` `service nfs status`  
`sudo service nfs-kernel-server start` `/etc/init.d/nfs-kernel-server start`  
`sudo service nfs-kernel-server restart` `/etc/init.d/nfs-kernel-server restart` #rpi    
`sudo service nfs-kernel-server stop` `sudo service nfs-kernel-server stop` #rpi   
  
`nano /etc/exports`  
```
/mnt/nfs/ *(rw,sync,insecure,no_subtree_check)  
```
><sup>`rw` enable read and write</sup>  
><sup>`sync` prevents file corruption</sup>  
><sup>`insecure` needed when port number is higher than 1024</sup>  
><sup>`no_subtree_check` removes warning message when explicitly defined</sup>  

`exportfs -ra`  

`showmount -e`  
`rpcinfo -p | grep portmap`  

`cat /proc/filesystems | grep nfs`  
`rpcinfo -p | grep portmap`  
`service rpcbind restart`  

`reboot`  

---
`lsblk`

```
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0  3.7T  0 disk 
└─sda1        8:1    0  3.7T  0 part 
...
```

```
SERVER (LINUX)  
mount /dev/sda1 /mnt/SERVER  


apt-get install nfs-kernel-server  
service nfs status
service nfs-kernel-server start  

nano /etc/exports  

/mnt/SERVER *(rw,sync)  



CLIENT (MAC)  
sudo nfsd start  
sudo mount -t nfs 192.168.1.104:/mnt/SERVER ~/Desktop/nfs  
sudo mount -o rsize=8192,wsize=8192 -t nfs 192.168.1.104:/mnt/server1 ~/Desktop/NFS 
sudo umount ~/Desktop/nfs
sudo umount ~/NFS
sudo umount -l ~/NFS
```