#### debian minimal install amd64
---
ping loopback `ping 127.0.0.1`  


Pinging network for router with `ping -c1 192.168.1.1` returns errors.   
```
PING 192.168.1.1 (192.168.1.1) 56(84) bytes of data.
From 169.254.4.138 icmp_seq=1 Destination Host Unreachable
...
--- 192.168.1.1 ping statistics ---
6 packets transmitted, 0 received, +3 errors, 100% packet loss, time 5076ms
```

view the routing table with <b>`#`</b> `ip route`  
```
default via 192.168.1.1 dev wlp2s0
192.168.1.0/24 dev wlp2s0 proto kernel scope link src 192.168.1.114
```


---

verify wpasupplicant and wireless-tools have been installed for wifi devices  
<b>`$`</b> `apt policy wpasupplicant wireless-tools` otherwise,  
<b>`$`</b> `apt install wpasupplicant wireless-tools`  
``` 
lo        no wireless extensions

wlp2s0    IEEE 802.11  ESSID:off/any
          Mode:Managed  Access Point: Not-Associated   Tx-Power=15 dBm
          Retry short limit:7   RTS thr:off   Fragment thr:off
          Encryption key:off
          Power Management:off
```

ping router  
<b>`#`</b> `ping -c1 192.168.1.1`
```
PING 192.168.1.1 (192.168.1.1) 56(84) bytes of data.
From 169.254.X.XXX icmp_seq=1 Destination Host Unreachable
...
--- 192.168.1.1 ping statistics ---
6 packets transmitted, 0 received, +3 errors, 100% packet loss, time 5076ms
```
><i>here, the destination host is unreachable.</i>

scan for visible ssids within range  
<b>`$`</b> `iw wlp2s0 scan | grep "SSID: *.[a-z]"`    

###### connecting  
`wpa_passphrase networkname password > /etc/wpa_supplicant/wpa_supplicant.conf`  
`nano /etc/wpa_supplicant/wpa_supplicant.conf`  

# 
`nano /etc/network/interfaces`  
```
...
auto wlan0
iface wlan0 inet dhcp
    wpa-ssid "networkname"
    wpa-psk "password"
```

``` 
lo        no wireless extensions

wlp2s0    IEEE 802.11  ESSID:"networkname"
          Mode:Managed  Frequency:2.412 Ghz Access Point: A3-3B-73-40-E8-AC   
          Bit Rate=52 Mb/s   Tx-Power=15 dBm
          Retry short limit:7   RTS thr:off   Fragment thr:off
          Encryption key:off
          Power Management:off
          Link Quality=70/70  Signal level=-11 dBm
          Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0
          Tx excessive retries:0  Invalid misc:48   Missed beacon:0
```

<b>`#`</b> `ip route`  
```
default dev wlp2s0 scope link metric 1002 linkdown
169.254.0.0/16 dev wlp2s0 proto kernel scope link src 169.254.4.138 linkdown
```

<b>`#`</b> `ip route`  
```
default via 192.168.1.1 dev wlp2s0
192.168.1.0/24 dev wlp2s0 proto kernel scope link src 192.168.1.114
```

`ifconfig eth0 192.168.0.1 netmask 255.255.255.0`  
`route`
`route -n`  
`route add default gw 192.168.0.253 eth0` 
`route add -net 192.168.1.0/24 gw 192.168.1.1 wlp2s0`  
`hostname -I`  

`cat /etc/resolv.conf`  
`cat /etc/network/interfaces`  

`iwconfig`  
`iwconfig wlp2s0 essid "wifi-ssid-name"`   
`hostname -I`  
`ping -c1 192.168.1.1`  

`ip a`  
`ip route`  
`ip route show`  
`ip route add default via 192.168.1.1 dev wlp2s0`  
`ip addr show`  
`ip link set wlp2s0 down`  
`ip link set wlp2s0 up`  
`ip link show wlp2s0`  
`iw dev wlp2s0 link`  
`iw dev wlp2s0 connect "wifi-ssid-name"`  

 Letter | Digit | Character
 ------ | ------|----------
 a      | 4     | $
        | 365   | (
 b      |       | ^  

---
---
`ping 127.0.0.1`

`ping -c1 192.168.1.1`
```
 Destination Host Unreachable

```
# confirm wireless tools
`apt policy wpasupplicant wireless-tools`
`apt install wpasupplicant wireless-tools`

`nano /etc/network/interfaces`

```
auto lo
iface lo inet loopback

```

---
For password-protected WPA connections, `wpasupplicant` will be required.
For WEP and open do not require this next step.

`nano /etc/network/interfaces`
```
auto lo
iface lo inet loopback
```

```
auto wlan0
iface wlan0 inet dhcp
    wpa-ssid "networkname"
    wpa-psk "password"

```


`wpa_passphrase networkname password > /etc/wpa_supplicant/wpa_supplicant.conf`
`nano /etc/wpa_supplicant/wpa_supplicant.conf`

```
su
ip route show
ip link set wlp2s0 down
ip route add default via 192.168.1.1 dev wlp2s0
iw dev wlp2s0 connect "wifi-ssid-name"
ip link set wlp2s0 up
# wait about 10 seconds
iw dev wlp2s0 link
ip route show
exit

```

$ ip route add default via 192.168.1.1 dev wlp2s0
  # RTNETLINK answers: Operation not permitted
$ sudo

$ ip route add default via 192.168.1.1 dev wlp2s0
  # RTNETLINK answers: Network is unreachable
$ sudo





dpkg -S `which ip`               # iproute2
dpkg -S `which iw`               # iw

dpkg -S `which iwconfig`         # wireless-tools
dpkg -S `which ifconfig`         # net-tools

dpkg -S `which wpa_passphrase`   # wpasupplicant

###### routing table

`route -n`  
```
Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
0.0.0.0         192.168.1.1     0.0.0.0         UG        0 0          0 wlan0
192.168.1.0     0.0.0.0         255.255.255.0   U         0 0          0 wlan0
```
The routing table rules read from top to bottom. Routes on the top of the table are more specific than routes toward the bottom.

Destination - What range of addresses do you want to reach?
Gateway - Which router do you want to use to reach the destination?
Genmask - This defines the subnet
Flags - 
MMS - 
Window - 
irtt - 
Iface - 

```
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         192.168.1.1     0.0.0.0         UG    0      0        0 eth0
172.17.0.0      0.0.0.0         255.255.0.0     U     0      0        0 docker0
192.168.1.0     0.0.0.0         255.255.255.0   U     0      0        0 eth0
```
`ping 192.168.1.255`  
`cat /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts`  
`echo 0 > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts`  

this worked after setting up wpa_supplicant
```
ip link set wlp2s0 down
iw dev wlp2s0 connect "wifi-ssid-name"
ip link set wlp2s0 up
# wait about 10 seconds
```

