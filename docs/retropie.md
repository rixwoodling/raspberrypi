### RETROPIE
<i>steps to configure Retropie on a Raspberry Pi 3 for an emulated CRT look with an original NES controller</i> 

emulationstation serves as the graphical os for retropie, in control of menu navigation, themes, and even the retropie startup screen. 
retroarch handles communcation and configurations set by the user to the lower level libraries and emulators. 
libretro is an API with a libretro core as a basic front end. 
retropie is simply a debian-based Linux distro which everything sits on top of. 

---
#### installation and hardware checks
manually install, use raspberry pi imager, or if downloading retropie image,<br> 
download, then
```
gzip -d -c retropie-buster-4.8-rpi2_3_zero2w.img.gz | dd of=/dev/sdX
```
insert sd card into raspberry pi and boot<br>
ensure power cable and DC 5.1 V 2.5 A power supply are not causing blinking red led power issues.<br>
```
vcgencmd get_throttled
```
for example, if the result is `throttled=0x50005`, convert each digit after the x to 4-bit binary, 
which converts to `0101 0000 0000 0000 0101` 
```
0: under-voltage
1: arm frequency capped
2: currently throttled 
16: under-voltage has occurred
17: arm frequency capped has occurred
18: throttling has occurred
```

---

`S Update RetroPie-Setup script`<br> 
`I Basic install`<br>
`M Manage packages` `opt Manage optional packages` `scraper` `S Install from source`*`C Configuration / Options`<br>
<i>* close emulationstation before config/options page</i>

---
### unicornhat pwm conflict
when using unicornhat on raspberry pi, the PWM conflicts with I2C or something like that. what happens is when hardware acceleration is used for video layout and sound is being delivered to analog output, all leds turn on with full brightness! the fix here is when using hw acceleration is to turn off analog out in emulationstation.<br> 

turn on hw acceleration: `Main Menu` `Other Settings` `Use OMX Player (HW Accelerated)`<br>
turn off analog output: `Main Menu` `Sound Settings` `OMX Player Audio Device`<br>

restart emulationstation

---
#### system configuration
configure controller<br>
`main menu` via start button `configure input`<br>

enable ssh ( from emulationstation ui )<br> 
`retropie (configuration)` `raspi-config` `interfacing options` `ssh`<br>

change themes<br>
`retropie (configuration)` `es themes` `install retropie/color-pi` for example, then<br>
`main menu` via start button `ui settings` `theme set`<br>

run command configurations
`retropie (configuration)` `run command configuration`, then<br>
set Launch menu to disabled ( if not making any more changes to cores )<br>
set Launch menu art to disabled<br>
Exit and restart emulationstation 

---
#### global configuration
every rom can have customized overlays and controller settings, but it depends on the boolean set for global_core_options. 

```
cat /opt/retropie/configs/all/retroarch.cfg | grep "global_core_options"
```
> if global_core_options is set to `false`, commented out, or missing, all system and rom configurations will have no effect. 
```
cat /opt/retropie/configs/arcade/retroarch.cfg
```
> if global_core_options is set to `true` for a system, configurations for each rom are allowed. otherwise, this config defers back to global.

---
#### rom configuration
every rom can have customized overlay and controller settings, but it depends on what is set for global_core_options. 


---
#### controllers
button assignment and general controller validation can be tested with `jstest`
```
jstest /dev/input/js0
```
```
Driver version is 2.1.0.
Joystick (USB Gamepad ) has 2 axes (X, Y)
and 10 buttons (Trigger, ThumbBtn, ThumbBtn2, TopBtn, TopBtn2, PinkieBtn, BaseBtn, BaseBtn2, BaseBtn3, BaseBtn4).
Testing ... (interrupt to exit)
Axes:  0:     0  1:     0 Buttons:  0:off  1:off  2:off  3:off  4:off  5:off  6:off  7:off  8:off  9:off 
```

adding roms
rom naming convention
rom configuration
```
cat /opt/retropie/configs/all/retroarch.cfg | grep "global_core_options"
```
```
cat /opt/retropie/configs/arcade/retroarch.cfg | grep "global_core_options"
```

```
# galaga
# lr-mame2003
# 1080p

# overlay info
input_overlay_enable = true
input_overlay = "~/RetroPie/roms/arcade/overlays/galaga.cfg"

# tv/monitor resolution
video_fullscreen_x = 1920
video_fullscreen_y = 1080

# 20 = Config, 21 = 1:1 PAR, 22 = Core Provided, 23 = Custom Viewport
aspect_ratio_index = "23"
custom_viewport_width = "480"
custom_viewport_height = "720"
custom_viewport_x = "720"
custom_viewport_y = "240"

# custom video shader
video_shader_enable = "true"
video_shader = "/opt/retropie/emulators/retroarch/shader/crt-pi-curvature-vertical.glslp"
```

```
# video_scale_integer = true
# input_overlay_opacity = 1.0
# input_overlay_scale = 1.0

# video_allow_rotate = "true"
# video_rotation = "1"

```
```
# galaga

overlays = 1
overlay0_overlay = galaga.png
overlay0_rect = "0.2,0.0,0.6,1.0"
overlay0_full_screen = true
overlay0_descs = 0
```

```
/opt/retropie/configs/all/retroarch.cfg         # global config
/opt/retropie/configs/SYSTEMNAME/retroarch.cfg  # system specific
```


```
# player 1 nes controller config
input_player1_joypad_index = 0
input_player1_a_btn = 1
input_player1_b_btn = 2
input_player1_select_btn = 8
input_player1_start_btn = 9
input_player1_up_axis = -1
input_player1_down_axis = +1
input_player1_left_axis = -0
input_player1_right_axis = +0

input_enable_hotkey_btn = 8 # select
input_exit_emulator_btn = 9 # select + start
input_save_state_btn = 5 
input_load_state_btn = 4
input_menu_toggle_btn = 0
input_state_slot_increase_axis = +0 # select + left d-pad
```
```
input_player2_joypad_index = 1
input_player2_b_btn = 2
input_player2_a_btn = 1
input_player2_y_btn = 3
input_player2_x_btn = 0
input_player2_l_btn = 4
input_player2_r_btn = 5
input_player2_start_btn = 9
input_player2_select_btn = 8

# nes d-pad config for player 1 and 2
input_player1_up_axis = -1
input_player1_down_axis = +1
input_player1_left_axis = -0
input_player1_right_axis = +0
input_player2_up_axis = -1
input_player2_down_axis = +1
input_player2_left_axis = -0
input_player2_right_axis = +0

input_enable_hotkey_btn = 8 # select
input_exit_emulator_btn = 9 # select + start
input_save_state_btn = 5 
input_load_state_btn = 4
input_menu_toggle_btn = 0
input_state_slot_increase_axis = +0
input_state_slot_decrease_axis = -0

```


3B modest overclock
```
arm_freq_min=900
core_freq_min=325
```

overclocking settings
```
arm_freq=1350
gpu_freq=500
core_freq=500
sdram_freq=500
sdram_schmoo=0x02000020
over_voltage=4
sdram_over_voltage=3
v3d_freq=525
```
```
arm_freq=1350
gpu_freq=525
core_freq=525
sdram_freq=500
over_voltage=6
v3d_freq=525
force_turbo=1
avoid_pwm_pll=1
disable_splash=1
```

#### psx
emulator: lr-pcsx-rearmed

---
#### achievements
first, create account at https://retroachievements.org <br>
next, QUIT EMULATION STATION<br>
then, in retropie<br>
```
sudo nano -l /opt/retropie/configs/all/retroarch.cfg
```
set some features to true, remove #, and add "username" and "password" in quotes 
```
cheevos_enable = true
cheevos_username = "YourUsername"
cheevos_password = "YourPassword"
cheevos_auto_screenshot = false
cheevos_leaderboards_enable = false
cheevos_start_active = true
```
start emulationstation

---


