##### Creating User and Group

`useradd -s /bin/bash -m foo`  
`groupadd bar`  
`groups foo`  
`usermod -a -G admin foo`  
`ls -a /home/foo                     # .bash_logout  .bashrc  .profile  `  
```
.  ..  .bash_logout  .bashrc  .profile
```
`cd /etc/skel`  
`ls -a`  

`passwd rix`  
```
Enter new UNIX password: 
Retype new UNIX password: 
passwd: password updated successfully
```
---

`userdel --remove foo`  
`groupdel bar`  


---

`id`
```
uid=0(root) gid=0(root) groups=0(root)
```
```
uid=1000(user) gid=1000(user) groups=1000(user),1001(admin)
```

`cat /etc/passwd | grep user`
```
root:x:0:0:root:/root:/bin/bash
```
```
user:x:1000:1000::/home/user:/bin/bash
```

`groups`
```
root
```
```
user admin
```
`cat /etc/group | grep root`
```
root:x:0:
```
```
user:x:1000:
admin:x:1001:user
```

---
<sup>`?` `https://renenyffenegger.ch/notes/Linux/shell/commands/useradd`</sup>  

