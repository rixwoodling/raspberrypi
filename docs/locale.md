##### Setting Locale

<sup><i>Locales are used by glibc and other locale-aware programs or libraries for 
rendering text, correctly displaying regional monetary values, time and date 
formats, alphabetic idiosyncrasies, and other locale-specific standards.</i></sup>  

---
###### Setting locale for non-interactive shell (root)

`1` Verify default locale (as root) `locale`  
><sub><i>The locale may show 'en_GB.UTF-8' as the default language.  
Changing these values can be done at `nano /etc/default/locale` but not without installing and generating locales first.</i></sub>  

`2` Uncomment locales wanted `nano /etc/locale.gen`</sup>  
><sup><i>For example 'en_US.UTF-8' and zh_TW.UTF-8</i></sup>  

`3` Generate locales with locale-gen, which installs languages `locale-gen`  

`4` Verify locales have been installed `locale -a`  

`5` Set system-wide locale `nano /etc/default/locale`  
```
LC_ALL=en_US.UTF-8 
```
```
LANG=en_US.UTF-8 
```
```
LANGUAGE=zh_TW:fr_FR:en_US 
LC_TIME="fr_FR.utf8" 
LC_MESSAGES="zh_TW.utf8" 
LANG="en_US.UTF-8"
```
><sup><i>If other languages are used, they will also need to pass steps 2 and 3.</i></sup>  
><sup><i>LANG is the same as LC_ALL, but LANG can be overridden by other variables. LC_ALL will override everything.</i></sup>  
><sub><i>Only languages generated from locale-gen can be added to `nano /etc/default/locale`.  
Proper locale names can also be verified in `cat /usr/share/i18n/SUPPORTED.`</i></sub>

`6` Disable locale import over ssh by commenting out AcceptEnv LANG LC_* `nano /etc/ssh/sshd_config`  
```
# AcceptEnv LANG LC_*
```

><sup><i>Root-level changes to locale must be rebooted. Then, log back into root.</i></sup>  

`7` Reboot `reboot now`  
><sup><i>Root-level changes to locale must be rebooted. Then, log back into root.</i></sup>  

`8` Verify locale changes `locale`    
&nbsp;

---
###### Locale Environment Variables

><sup>`LANGUAGE` Sets language environment. Can be used as fallback. Does not set other variables.</sup>  
><sup>`LC_ALL` Overrides all environment variables. Should exist alone.</sup>  
><sup>`LC_CTYPE` Character classification and case conversion.</sup>  
><sup>`LC_NUMERIC` Non-monetary numeric formats. # en_US displays 12,345.00 while fr_FR displays 12 345,00.</sup>  
><sup>`LC_TIME` Date and time formats. en_US prints 'Sat Mar 26...' but fr_FR prints 'samedi 26 mars...'' for `date` command.</sup>  
><sup>`LC_COLLATE` Sort order. Setting the value to C sorts in the order of dotfiles, uppercase then lowercase.</sup>  
><sup>`LC_MONETARY` Monetary formats.</sup>  
><sup>`LC_MESSAGES` Formats of informative and diagnostic messages, and of interactive responses.</sup>  
><sup>`LC_PAPER` Paper size.</sup>  
><sup>`LC_NAME` Name formats.</sup>  
><sup>`LC_ADDRESS` Address formats and location information.</sup>  
><sup>`LC_TELEPHONE` Telephone number formats.</sup>  
><sup>`LC_MEASUREMENT` Measurement units (Metric or Other).</sup>  
><sup>`LC_IDENTIFICATION` Metadata about the locale information.</sup>  
><sup>`LANG` The default value, which is used when either LC_ALL is not set, or an applicable value for LC_* is not set.</sup>  

---
<sub>`?` https://wiki.archlinux.org/index.php/locale</sub>  
<sub>`?` http://unix.stackexchange.com/questions/110757/locale-not-found-setting-locale-failed-what-should-i-do)</sub>  
<sub>`?` https://rohankapoor.com/2012/04/americanizing-the-raspberry-pi/)</sub>  
<sub>`?` https://people.debian.org/~schultmc/locales.html)</sub>  
<sub>`?` http://askubuntu.com/questions/21316/how-can-i-customize-a-system-locale)</sub>  
<sub>`?` https://linuxprograms.wordpress.com/tag/c-utf-8-handling/)</sub>  
<sub>`?` http://pubs.opengroup.org/onlinepubs/009695399/basedefs/xbd_chap07.html)</sub>  
<sub>`?` https://www.gnu.org/software/gettext/manual/html_node/Locale-Environment-Variables.html)</sub>  
<sub>`?` https://www.gnu.org/savannah-checkouts/gnu/libc/manual/html_node/Locale-Categories.html)</sub>  
<sub>`?` https://www.gnu.org/software/gettext/manual/html_node/Aspects.html#Aspects)</sub>  
<sub>`?` http://stackoverflow.com/questions/9374868/number-formatting-in-bash-with-thousand-separator)</sub>  
<sub>`?` http://www.cyberciti.biz/faq/unix-linux-bash-number-formatting-in-with-thousand-separator/)</sub>  
<sub>`?` https://wiki.archlinux.org/index.php/locale)</sub>  
<sub>`?` http://unix.stackexchange.com/questions/149111/what-should-i-set-my-locale-to-and-what-are-the-implications-of-doing-so)</sub>  
<sub>`?` https://help.ubuntu.com/community/Locale)</sub>  
<sub>`?` http://unix.stackexchange.com/questions/149348/how-to-display-chinese-characters-correctly-on-remote-red-hat-machine)</sub>  
<sub>`?` https://isis.poly.edu/~qiming/chinese-debian-mini-howto.html)</sub>  
<sub>`?` https://www.ibm.com/support/knowledgecenter/en/SSMKHH_9.0.0/com.ibm.etools.mft.doc/ae19494_.htm</sub>  
