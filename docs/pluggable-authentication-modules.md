## Pluggable Authentication Modules / PAM


---
### password complexity  
set the minimum password length to 8 characters
```
cat /etc/pam.d/common-password | grep ^password
```
```
password [success=2 default=ignore] pam_unix.so obscure sha512 minlen=8
```

##### Password Aging
---
Set password expiration for new users to 90 days, the minimum number of days a password can be changed with 0 being unlimited, and 7 days warning given to users before password expires.     
`#` `cat /etc/login.defs | grep ^PASS`  
```
PASS_MAX_DAYS	90
PASS_MIN_DAYS	0
PASS_WARN_AGE	7
```
---



`ls /etc/pam.d/`  
`cat /etc/pam.d/sshd` 
```  
 account  required  pam_nologin.so
```
`ls /etc/security/`  

`2` password complexity  
`apt policy libpam-pwquality`  
`apt install libpam-pwquality`  
`nano /etc/pam.d/common-password`  
```
password requisite pam_pwquality.so retry=3 ucredit=-1     # 1 uppercase  
password requisite pam_pwquality.so retry=3 lcredit=-1     # 1 lowercase  
password requisite pam_pwquality.so retry=3 dcredit=-1     # 1 digit  
password requisite pam_pwquality.so retry=3 ocredit=-1     # 1 symbol 
```

