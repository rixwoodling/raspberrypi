##### Setting Localtime

<i>The /etc/localtime file configures the system-wide timezone of the local 
system.</i>    

---
###### Set Localtime
`1` Copy a location preset to `etc/localtime`.  
>`$` `cp /usr/share/zoneinfo/Asia/Taipei /etc/localtime`  
>`$` `cp /usr/share/zoneinfo/America/Los_Angeles /etc/localtime`  
>`$` `cp /usr/share/zoneinfo/Europe/Paris /etc/localtime`  

`2` Verify timezone change by using `cat /etc/timezone`.  
><sup>`#` Alternatively use `timedatectl` as well as `date + Z% z%`</sup>   
><sup>`#` If timezone does not change, use `dpkg-reconfigure tzdata`</sup>   

<sup>`!` <i>`localtime` and the location presets that replace `/etc/localtime` 
are in a binary format and cannot be edited.</i></sup>  

---
<sup>`?` <i>https://www.garron.me/en/linux/set-time-date-timezone-ntp-linux-shell-gnome-command-line.html</i></sup>  
<sup>`?` <i>https://www.raspberrypi.org/forums/viewtopic.php?t=4977&f=5</i></sup>    
<sup>`?` <i>http://man7.org/linux/man-pages/man5/localtime.5.html</i></sup>  
