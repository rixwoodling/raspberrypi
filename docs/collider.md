### setting up collider


#### prerequisites

install go  
checkout apprtc repo 
```
git clone https://github.com/webrtc/apprtc.git ~/apprtc
```

create go workspace
```
mkdir ~/goWorkspace
export GOPATH=$HOME/goWorkspace/ 
echo export GOPATH=$HOME/goWorkspace/ >> ~/.profile
mkdir $GOPATH/src
```

link collider files from apprtc to newly created src folder in goWorkspace
```
ln -s $HOME/apprtc/src/collider/collider $GOPATH/src
ln -s $HOME/apprtc/src/collider/collidermain $GOPATH/src
ln -s $HOME/apprtc/src/collider/collidertest $GOPATH/src
```


```
cd ~/go/src
go get collidermain

