##### Message of the Day

<sup><i>Locales are used by glibc and other locale-aware programs or libraries for 
rendering text, correctly displaying regional monetary values, time and date 
formats, alphabetic idiosyncrasies, and other locale-specific standards.</i></sup>  


---
###### Setting locale for non-interactive shell (root)  

`nano /etc/motd`  
`nano /etc/profile.d/motd.sh`  

```
#! /bin/bash

# uptime
upSeconds="$(/usr/bin/cut -d. -f1 /proc/uptime)"
secs=$((${upSeconds}%60))
mins=$((${upSeconds}/60%60))
hours=$((${upSeconds}/3600%24))
days=$((${upSeconds}/86400))
UPTIME=`printf "%d days, %02dh%02dm%02ds" "$days" "$hours" "$mins" "$secs"`

# get the load averages
read one five fifteen rest < /proc/loadavg

echo "
`date +"%a %b %e %Y PDX.%Z %R%p TPE.%Z %R%p"`
`uname -srmo` \
`free -m | awk 'NR==2{printf "Mem%sMB:%.2f%% ", $2,$3*100/$2 }'`\
`df -h | awk '$NF=="/"{printf "Disk:%d/%dGB (%s) ", $3,$2,$5}'`
`top -bn1 | grep load | awk '{printf "Load:%.2f ", $(NF-2)}'`\
`printf Up:"%dd%02dh" "$days" "$hours"`
Mem:`cat /proc/meminfo | grep MemFree | awk {'print $2'}`kB (Free) / `cat /proc$
CPU:`nproc` Load:${one}:${five}:${fifteen} Proc:`ps ax | wc -l | tr -d " "`
IP Addresses.......: `ip a | grep glo | awk '{print $2}' | head -1 | cut -f1 -d$
"
free -m | awk 'NR==2{printf "Mem%sMB:%.2f%% ", $2,$3*100/$2 }'
df -h | awk '$NF=="/"{printf "Disk:%d/%dGB (%s) ", $3,$2,$5}'
top -bn1 | grep load | awk '{printf "Load:%.2f\n", $(NF-2)}'
#⌁
echo `printf vcgencmd measure_volts core`

# ▁ ▂ ▃ ▅ ▆ ▉
# cpu:▁


```


<sub>`?` <i>https://ownyourbits.com/2017/04/05/customize-your-motd-login-message-in-debian-and-ubuntu/</i></sub>  
<sub>`?` <i>https://unix.stackexchange.com/questions/119126/command-to-display-memory-usage-disk-usage-and-cpu-load</i></sub>  