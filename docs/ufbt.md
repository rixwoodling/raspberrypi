##ufbt

https://instantiator.dev/post/flipper-zero-app-tutorial-01/<br>
https://instantiator.dev/post/flipper-zero-app-tutorial-02/<br>
https://www.youtube.com/watch?v=CLsLZO15S44<br>

```
cd ~/git
```
```
git clone https://github.com/flipperdevices/flipperzero-ufbt.git
```
```
python3 -m pip install --upgrade ufbt
```
--channel=[dev|rc|release]
```
ufbt update --channel=dev
```

ufbt flash_usb
ufbt flash

mkdir skeleton
cd skeleton
ufbt create APPID=skeleton

application.fam
skeleton.c
skeleton.png
images/

build the app (creates dist dir)
```
ufbt
```

### minicom
```
sudo apt-get install minicom
```
```
sudo dmesg | grep tty
```
replug to get motd
```
sudo minicom -o -D /dev/ttyACM0 -b 115200 || \
sudo minicom --noinit --device /dev/ttyACM0 --baudrate 115200 || \
sudo minicom -D /dev/serial/by-id/$(ls /dev/serial/by-id/)
```
```
subghz chat 314527389 0
```

to quit:
`Ctrl+A`<br>
`X` (`Q` + `enter`)<br>
`Z` (`Q`)<br>  



screen /dev/ttyACM0 115200
