debian amd64 usb install


# create mount folder, then download iso and wireless firmware to folder
mkdir ~/tmp
wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.6.0-amd64-netinst.iso ~/tmp
wget http://ftp.us.debian.org/debian/pool/non-free/f/firmware-nonfree/firmware-iwlwifi_20200918-1_all.deb ~/tmp
dpkg-deb --extract firmware-iwlwifi_20200918-1_all.deb ~/tmp

# erase usb MBR, and dd to usb
dd if=/dev/zero of=/dev/sda bs=512 count=1 seek=0
dd if=~/sda/*.iso of=/dev/sda status=progress

# create a new partition, reload/verify partition tables, then create filesystem
fdisk /dev/sda
partx -a /dev/sda || sudo hdparm -z /dev/sda && cat /proc/partitions
mkfs -t ext3 /dev/sda3

# mount new partition and copy all files to root of partition


---
# insert usb to computer, boot from bios (f2)
# start install process
# switch 

