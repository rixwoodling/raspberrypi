##### Connecting to the Raspberry Pi via SSH  

<sup><i>This is a tutorial on how to connect to the Raspberry Pi from either a Mac or Linux using SSH.</i></sup>  

---
`1` Find the IP of the RPi on the home network from a host system with fing or nmap. `sudo fing`

> <sup>`!`<i> Verify that the Ethernet cable is connected as the barebones build won't have support for wireless until wireless tools are setup later on.</i></sup>  
> <sup>`!`<i> If a Discovery error: unable to select a valid network interface to use message appears, turn off VPN.</i></sup>  
> <sup>`!`<i> The PWR LED should be a solid red, assuming the RPi is powered properly with 5V.</i></sup>  
> <sup>`!`<i> The ACT LED (green colored) should not be blinking during an idle state, otherwise indicating an issue with the SD card.</i></sup>  

`2` Log into the RPi. `ssh root@192.168.X.XXX`

> <sup>`!`<i> Replace the X'd out IP with the IP found using on of the network scanners.*</i></sup>  
> <sup>`!`<i> SSH into 'root'. For this barebones build, there is no 'pi' user as there is with a full Raspbian setup.</i></sup>  
> <sup>`!`<i> The default password is raspberry.</i></sup>  
> <sup>`!`<i> If a WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED! message appears, correct the host key with ssh-keygen -R 192.168.X.XXX.*</i></sup>  

<sup>`#`<i> If logged in successfully, the terminal prompt should display root@raspberrypi:~#.</i></sup>  
<sup>`#`<i> To close the connection, type exit.</i></sup>

---
<sup>`?` https://www.raspberrypi.org/documentation/remote-access/ssh/unix.md</sup>  