###### Section 01: Essential Commands  
`ssh -p 22 root@192.168.X.XXX                        ` # log into local & remote graphical and text mode consoles  
`find / -type f -mtime +1 -name "*.log"              ` # search for files  
`                                                    ` # evaluate and compare the basic file system features and options  
`diff -Nur A.txt B.txt > C.txt && patch A.txt C.txt  ` # compare and manipulate file content  
`echo hi | tee 2>/dev/null >> file                   ` # use input-output redirection (e.g. >, >>, |, 2>)  
`grep -v '^[!0-9]' ./data.csv | awk -F, '{print $1}' ` # analyze text using basic regular expressions  
`tar -cjvf backup.tar.gz data && tar -xjvf *.tar.gz  ` # archive, backup, compress, unpack, and uncompress files  
`mkdir foo && mv foo bar && rm -ifr bar              ` # create, delete, copy, and move files and directories  
`ln -s source target                                 ` # create and manage hard and soft links  
`chmod u+rwx,go=rxt data                             ` # list, set, and change standard file permissions  
`man man || info man                                 ` # read, and use system documentation  
`echo "%admin ALL=(ALL:ALL) ALL" >> /etc/sudoers     ` # manage access to the root account  

###### Section 02: Operation of Running Systems  
`shutdown now || journalctl poweroff                 ` # boot, reboot, and shut down a system safely  
`runlevel && telinit 3 || systemd.unit=rescue.target ` # boot or change system into different operating modes  
`grub-install /dev/sdX                               ` # install, configure and troubleshoot bootloaders  
`ps aux # kill -9 script                             ` # diagnose and manage processes  
`find / -name "*.log" 2>/dev/null                    ` # locate and analyze system log files  
`cd /var/spool/cron/crontabs/ && crontab -e          ` # schedule tasks to run at a set date and time  
`                                                    ` # verify completion of scheduled jobs  
`apt update && apt upgrade                           ` # update software to provide required functionality and security  
`ps -eo pid,ppid,cmd,%cpu,%mem --sort=-%cpu          ` # verify the integrity and availability of resources  
`                                                    ` # verify the integrity and availability of key processes  
`echo 0 > /proc/sys/net/ipv4/ip_forward # sysctl     ` # change kernel runtime parameters, persistent and non-persistent  
`                                                    ` # use scripting to automate system maintenance tasks  
`service --status-all                                ` # manage the startup process and services (In Services Configuration)  
`id -eZ                                              ` # list and identify SELinux/AppArmor file and process contexts  
`apt install pgrm && apt policy pgrm                 ` # manage software  
`file data.csv || type data.csv                      ` # identify the component of a Linux distribution that a file belongs to  

###### Section 03: User and Group Management  
`useradd -m foo && userdel --remove foo              ` # create, delete, and modify local user accounts  
`usermod -aG bar foo && deluser -d foo               ` # create, delete, and modify local groups and group memberships  
`                                                    ` # manage system-wide environment profiles  
`                                                    ` # manage template user environment  
`echo "* hard nproc 10" >> /etc/security/limits.conf ` # configure user resource limits  
`                                                    ` # manage user privileges  
`ls /etc/pam.d /etc/security                         ` # configure PAM  

###### Section 04: Networking  
`                                                    ` # configure networking and hostname resolution statically or dynamically  
`                                                    ` # configure network services to start automatically at boot  
`iptables -I INPUT -s 10.0.0.0/24 -d port 25 -j DROP ` # implement packet filtering  
`ip addr show && ip link set eth0 up                 ` # start, stop, and check the status of network services  
`ip route add 10.0.0.0/24 via 192.168.0.1 dev eth0   ` # statically route IP traffic  
`                                                    ` # synchronize time using other network peers  

###### Section 05: Service Configuration  
`                                                    ` # configure a caching DNS server  
`                                                    ` # maintain a DNS zone  
`                                                    ` # configure email aliases  
`                                                    ` # configure SSH servers and clients  
`                                                    ` # restrict access to the HTTP proxy server  
`                                                    ` # configure an IMAP and IMAPS service  
`                                                    ` # query and modify the behavior of system services at various operating modes  
`                                                    ` # configure an HTTP server  
`                                                    ` # configure HTTP server log files  
`                                                    ` # configure a database server  
`                                                    ` # restrict access to a web page  
`lxc-create -t alpine -n myc # lxc-start lxc-console ` # manage and configure containers  
`qemu                                                ` # manage and configure Virtual Machines  

###### Section 06: Storage Management  
`                                                    ` # list, create, delete, and modify physical storage partitions  
`pvcreate /dev/sdc && vgcreate /dev/sdc # lvcreate   ` # manage and configure LVM storage  
`                                                    ` # create and configure encrypted storage  
`echo '/dev/sda1 /mnt ext3 defaults 0 0' >>/etc/fstab` # configure systems to mount file systems at or during boot  
`                                                    ` # configure and manage swap space  
`                                                    ` # create and manage RAID devices  
`                                                    ` # configure systems to mount file systems on demand  
`                                                    ` # create, manage and diagnose advanced file system permissions  
`                                                    ` # etup user and group disk quotas for filesystems  
`                                                    ` # create and configure file systems  


