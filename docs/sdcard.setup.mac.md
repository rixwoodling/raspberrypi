##### Installation Notes (Mac)

<sup><i>This is a tutorial on how to install a barebones Linux image onto an SD card from a Mac.</i></sup>  

---

`1` Insert SD card. With Terminal open, type `diskutil list` or `df -h`.  
> <sup><i>Work out the location of the SD card, for example might be:</i> `/dev/disk3s1`.</sup>  

`2` Unmount the SD card so it can be erased. `diskutil unmount /dev/disk3s1`  
><sup><i> Step 2 can be skipped since the command in Step 3 also unmounts.</i></sup>  
><sup>`!`<i> Important! -- Note that </i>`disk3s1`<i> is now </i>`rdisk3`.</i></sup>  

`3` Erase the SD card. `diskutil partitionDisk /dev/disk3 1 MBR "Free Space" "%noformat%" 100%`  

`4` Write .img to the SD card. `sudo dd bs=1m if=*.img of=/dev/rdisk3`  
><sup>`*`<i> You must be in the folder containing .img file before writing.</i></sup>  
><sup>`!`<i> If the error </i>`dd: /dev/rdisk3: Permission denied`<i> comes up, see step 3.</i></sup>  
><sup>`!`<i> If the error </i>`dd: bs: illegal numeric value`<i> comes up, change </i>`bs=1m`<i> to </i>`bs=1M`.</sup>  

`5` Finally, eject the disk. `diskutil eject /dev/rdisk3`


---
<sup>`?`<i> https://www.raspberrypi.org/documentation/installation/installing-images/mac.md</i></sup> 

```
diskutil list
```
```
diskutil unmount /dev/disk3s1
```
```
diskutil partitionDisk /dev/disk3 1 MBR "Free Space" "%noformat%" 100%
```
```
sudo dd bs=1m if=*.img of=/dev/rdisk3
```

