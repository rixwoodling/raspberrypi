#### Dynamic IP Network Configuration

`eth0` `wlan0`

---
&nbsp;

This is the default network configuration located in /etc/network/interfaces.
This configuration offers the always present loopback and 
a simple ethernet config with dhcp, which automatically assigns 
a next-in-line local ip. No action is required to modify 
the default configuration for basic ethernet connection.

```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
allow-hotplug eth0

auto wlan0
allow-hotplug wlan0

iface wlan0 inet dhcp
   wpa-ssid "wifi_name"
   wpa-psk "wifi_password"
   
```

! With this configuration, ethernet must be plugged in if working on 
  a headless system. Hotplugging isn't supported either.


add this to interfaces if two routers are present
```
iface eth1 inet dhcp
post-up route add -host 10.1.2.51 eth1
post-up route add -host 10.1.2.52 eth1
```
