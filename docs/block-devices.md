###### block devices

A traditional hard drive is made up of tracks, cylinders, clusters, sectors, and Linux reads and writes data to these devices in blocks. Block devices, among other device types, are listed in the `/dev` directory, however these block types are represented differently in BSD systems. Block devices include hard drives, solid-state drives, cdrom drives, as well as loop devices. Disk management tools such as `fdisk`, `sfdisk`, `dd`, and `lsblk` are some of the utilites used to create, manipulate, analyze, repair, and erase disks.  

---

#### listing devices

`lsblk`

#### wiping a block device clean

delete all partitions and reset a drive to zero by using either `sfdisk` or `dd if=/dev/zero of=/dev/sdb`.<br>
if backing up whatever is on this drive sounds like a good idea, see "backing up a whole disk image"
```
sfdisk --delete /dev/sdb
```

---

#### create disk label

The "disk label" is the first layer which includes partition table information, such as the Master Boot Record. The disk formmatting command `sfdisk` does not create an empty partition table without creating a partition by default, thus, it must be explicitly stated. This command will appear as `/dev/sdb` in `lsblk` without any partitions:
```
echo 'label: dos' | sudo sfdisk /dev/sdb
```

---

#### create partitions


create Linux partition 

```
echo 'type=83' | sudo sfdisk /dev/sdb
```

create a W95 FAT16 (LBA) partition. `dosfstools` must be installed. 
```
echo 'type=e, size=10MiB, bootable' | sudo sfdisk /dev/sdb
```

###
```
echo 'start=22528, size=3600MiB, type=83' | sudo sfdisk /dev/sdb 2
```

---

#### backing up a whole disk image

create a backup of a while disk image, such as from a flash drive, to an img file:
```
sudo dd if=/dev/sdb of=~/img/mybackup.img bs=64k status=progress
```

restore the image to a drive:
```
sudo dd if=~/img/mybackup.img of=/dev/sdb bs=64k status=progress
```
><i>the block size of 64k should help resolve any input/output errors.</i>  
><i>add `status=progress` to show in progress activity.</i>  

---

#### mounting a block device

<p><i>
when a whole disk image is copied into an image file, it also includes the Master Boot Record.<br>
the `mount` command can only mount the block device without the MBR, otherwise a mount error is thrown.<br>
therefore, the `mount` command must be instructed to start at a sector calculated by multiplying the <b>start size</b> by the <b>sector size</b>.<br>
</i></p>

first, to do this, list the image information with fdisk:  
```
fdisk -l ~/img/mybackup.img  
```
```
Disk /home/rix/img/mybackup.img: 3.73 GiB, 4007657472 bytes, 7827456 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x8987aa84

Device                    Boot Start     End Sectors  Size Id Type
~/img/mybackup.img1             8064 7827455 7819392  3.7G  c W95 FAT32 (LBA)
```

then, mount the image as a loop device with:  
```
sudo mount -o loop,offset=4128768,uid=1000,gid=1000,umask=022 ~/img/mybackup.img
```
<i> if a "wrong fs type, bad option" error occurs, it's trying to mount with the MBR offset.</i>

---

#### create a multiboot usb

format a block device with FAT32, and verify block status with `lsblk` and `fdisk -l` 
```
sudo mkfs.vfat -I -F32 -n 'MBUSB' /dev/sdb
```
mount with writeable permissions
```
sudo mount -o uid=1000,gid=1000 /dev/sdb ~/mnt
```

---

#### create a bootable usb

```
Welcome to fdisk (util-linux 2.27.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

Device does not contain a recognized partition table.
Created a new DOS disklabel with disk identifier 0x55ecee0c.

Command (m for help): o
Created a new DOS disklabel with disk identifier 0xe648bbde.

Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (1-4, default 1): 1
First sector (2048-7553023, default 2048): enter
Last sector, +sectors or +size{K,M,G,T,P} (2048-7553023, default 7553023): +10M

Created a new partition 1 of type 'Linux' and of size 10 MiB.

Command (m for help): n
Partition type
   p   primary (1 primary, 0 extended, 3 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (2-4, default 2): 2
First sector (22528-7553023, default 22528): 
Last sector, +sectors or +size{K,M,G,T,P} (22528-7553023, default 7553023): enter

Created a new partition 2 of type 'Linux' and of size 3,6 GiB.

Command (m for help): t
Partition number (1,2, default 2): 1
Partition type (type L to list all types): e

Changed type of partition 'Linux' to 'W95 FAT16 (LBA)'.

Command (m for help): t
Partition number (1,2, default 2): 2
Partition type (type L to list all types): 83

Changed type of partition 'Linux' to 'Linux'.

Command (m for help): a
Partition number (1,2, default 2): 1

The bootable flag on partition 1 is enabled now.

Command (m for help): w
The partition table has been altered.
Syncing disks.
```

create boot format with `fdisk`
```
Disk /dev/sdb: 15.11 GiB, 16223567872 bytes, 31686656 sectors
Disk model: Storage Media   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x306f67d5

Device     Boot Start     End Sectors  Size Id Type
/dev/sdb1  *     2048   22527   20480   10M  e W95 FAT16 (LBA)
/dev/sdb2       22528 7553023 7530496  3.6G 83 Linux
```


```
sfdisk --label dos /dev/sdb
```
```
echo ',,c;' | sudo sfdisk /dev/sdb   # single vfat partition
```




| /dev/sda1 | /dev/loop0 |
| :- | :- |
| `dd if=/dev/zero of=mydisk bs=1024 count=1k` | `dd if=/dev/zero of=mydisk bs=1024 count=1k` |
| `losetup -f` | |
| `losetup /dev/loop0 mydisk` | |
| | |
| `fdisk /dev/loop0` | |
| `less /proc/partitions` | |
| `partprobe /dev/loop0` | | 



