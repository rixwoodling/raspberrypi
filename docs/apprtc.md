### building apprtc



```
# install docker
sudo apt install docker.io
```

```
# from pre-built a prebuilt docker image
sudo docker pull paiasy/apprtc-server
```
or
```
# building from a Dockerfile
git clone https://github.com/webrtc/apprtc.git ~/
sudo docker build ~/apprtc/
```

