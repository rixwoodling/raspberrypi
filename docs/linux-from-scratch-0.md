
### system requirements
```
sudo apt install \
bash binutils bison coreutils diffutils findutils gawk gcc \
grep gzip m4 make patch perl python sed tar texinfo xz-utils
```

create 10GB partition

---
### mount partition

setup 10GB ext4 partition onto mountpoint

```
sudo mkdir /mnt/lfs
```
```
sudo echo "/dev/sda3 /mnt/lfs ext4 defaults 1 1" >> /etc/fstab
```

add `LFS` variable to ~/.bashrc
```
echo 'export LFS=/mnt/lfs' >> ~/.bashrc
```
```
source ~/.bashrc
```

create `/mnt/lfs` mountpoint then mount partition
ensure `nosuid` and `nodev` are not set as option mounts
```
sudo mkdir -pv $LFS && \
sudo mount -v -t ext4 /dev/sdb $LFS
```

create limited directory layout
```
sudo mkdir -pv $LFS/{etc,var} $LFS/usr/{bin,lib,sbin} && \
for i in bin lib sbin; do sudo ln -sv usr/$i $LFS/$i; done && \
case $(uname -m) in x86_64) sudo mkdir -pv $LFS/lib64;; esac
```

---
### create lfs user and group

add lfs group, then<br>
add lfs user with bash shell, null skel, add to lfs group
```
sudo groupadd lfs && \
sudo useradd -s /bin/bash -g lfs -m -k /dev/null lfs
```

set lfs password
```
sudo passwd lfs
```

---
### setup tools and sources

create `$LFS/tools` directory, symlinked to `/tools`, change owner of `$LFS/tools` to lfs
```
sudo mkdir -v $LFS/tools && \
sudo ln -sv $LFS/tools / && \
sudo chown -v lfs $LFS/tools
```

create `$LFS/sources` directory, change owner to lfs, then make sticky
```
sudo mkdir -v $LFS/sources && \
sudo chown -v lfs $LFS/sources && \
sudo chmod -v a+wt $LFS/sources
```

download list of source packages into `$LFS/sources`
```
cd /mnt/lfs && \
wget http://www.linuxfromscratch.org/lfs/view/stable-systemd/wget-list && \
wget --input-file=wget-list --continue --directory-prefix=$LFS/sources
```

verify md5sum hashes for each source
```
wget http://www.linuxfromscratch.org/lfs/view/stable-systemd/md5sums && \
pushd $LFS/sources && \
md5sum -c md5sums && \
popd
```

---
### setup lfs login shell

login to lfs user
```
su - lfs
```

create `~/.bash_profile` read by the non-login shell
```
echo 'exec env -i HOME=$HOME TERM=$TERM PS1=\'\u:\w\$ \' /bin/bash' > ~/.bash_profile
```

and `~/.bashrc` read by the login shell
```
echo "set +h" > ~/.bashrc && \
echo "umask 022" >> ~/.bashrc && \
echo "LFS=/mnt/lfs" >> ~/.bashrc && \
echo "LC_ALL=POSIX" >> ~/.bashrc && \
echo "LFS_TGT=$(uname -m)-lfs-linux-gnu" >> ~/.bashrc && \
echo "PATH=/tools/bin:/bin:/usr/bin" >> ~/.bashrc && \
echo "export LFS LC_ALL LFS_TGT PATH" >> ~/.bashrc
```

source the non-login shell
```
source ~/.bash_profile
```




