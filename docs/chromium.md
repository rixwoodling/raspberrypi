# CHROMIUM

<p><i>https://chromium.googlesource.com/chromium/src/+/main/docs/linux/build_instructions.md</i></p>

---
### prerequisites

sudo priviledges will be needed for building things such as cros_sdk:
```
usermod -aG sudo $USER
```
```
umask 022
```
doing anything with git won't work without configuring your git config:
```
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```
chromiumos also requires [depot_tools](https://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/depot_tools_tutorial.html#_setting_up) for building, such as the cros_sdk environment:
```
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git && \
export PATH=/$HOME/depot_tools:$PATH
```

---
### setting up the repo

create chromium repo:
```
mkdir -p ~/chromium && \
pushd ~/chromium
```
fetch chromium repo
```
fetch --nohooks chromium
```
if errors occur, use:
```
gclient sync
```
change into src directory and check and install any missing build dependancies
```
cd src && \
./build/install-build-deps.sh
```
download additional binaries 
```
gclient runhooks
```

---
### building chromium
create build directory
```
gn gen out/default   # use this to create anew directory for patched build
```
build Chrome target with Ninja
```
autoninja -C out/default chrome
```


---
### patching a release branch

<p><i>https://www.chromium.org/developers/how-tos/get-the-code/working-with-release-branches/</i></p>

```
cd ~/chromium/src
```
```
git fetch --tags && \
git show-ref --tags | more
```
```
git checkout -b release-tag-5151-3 105.0.5151.3
```
>Updating files: 100% (47387/47387), done.<br>
>Previous HEAD position was ee99c84bf5ec4 Removing unused win10.20h2-blink-rel-dummy configs<br>
>Branch 'release-tag-5151-3' set up to track remote ref 'refs/tags/105.0.5151.3'.<br>
>Switched to a new branch 'release-tag-5151-3'<br>


create build directory for reference branch
```
gn gen out/default-5151-3
```
build reference branch
```
autoninja -C out/default-5151-3 chrome
```
patch reference branch ( commit not required to build )
```
patch -p1 < 9687059.diff
```
create build directory for patched branch
```
gn gen out/patched-5151-3
```
build patched branch
```
autoninja -C out/patched-5151-3 chrome
```



