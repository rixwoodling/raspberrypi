## taskset

When processes are run, they are not explicitly pinned to a given core. `taskset` is a scheduler property that can both retrieve and pin a process to a given set of CPUs on the system.
<br><br>

---
### show CPU affinity of a single process
to list the current CPU affinity of a process, for example `cras`, we can use either `top` or `ps -p $( pidof cras )`.<br>
let's say the pid for `cras` is `1648`:
```
taskset -p 1648
```
by default, this process will be set to the hexadecimal value of `fff` for CPUs 0-11, outputting:
```
# pid 1648's current affinity list: fff
```
by adding `--cpu-list` or `-c`, we can specify by CPU number instead:
```
taskset -cp 1648
```
this will output:
```
# pid 1648's current affinity list: 0-11
```
<br>

---
### setting CPU affinity of a single process
to pin process `1648` to CPUs 8-11 for example, we could either set this to the hexidecimal `f00`, or to `8-11` by using `-c` option.<br>
notice the format to pin a process to a set of CPUs: `taskset` `<option>` `<cpu>` `<pid>`
```
taskset -cp 8-11 1648
```
commas `,`, strides `:` and ranges `-`, also work and the example below is equivilant to just using `8-11`.
```
taskset -cp 8,9:10-11 1648
```
the output of either command will confirm the pid has been pinned to CPUs `8-11`:
```
# pid 1648's current affinity list: 0-11
# pid 1648's new affinity list: 8-11
```
<br>

---
### setting CPU affinity for multithreaded processes
considering our `cras` example, it actually has spawned a thread with pid `2158`. Some processes can spawn multiple threads. The effect of pinning CPU affinity for only one pid and excluding its threads may have little effect on performance value.
<br><br>
to change CPU affinity for a process as well as all of its threads, include the `--all-tasks` or `-a` option. in this command, we are changing the CPU affinity for all threads for pid `1648` and using `8-11` core list format.
```
taskset -acp 8-11 1648
```
assuming we are starting from a `0-11` CPU affinity, notice the output confirmation for both pids `1648` and `2158`.
```
# pid 1648's current affinity list: 0-11
# pid 1648's new affinity list: 8-11
# pid 2158's current affinity list: 0-11
# pid 2158's new affinity list: 8-11
```
<br>

---
### tasksetting specific chrome and os pids ( BKM )
in this example, we will change the CPU affinity of 3 specific chrome pids, and 3 os-specific pids to CPUs 8-11.<br>

`1` in the ChromeOS UI, start a Google Meet call. 
> a call does not need to connect to a second party for the needed pids to appear, so preview is fine.

`2` use the hotkeys `search` + `esc` to bring up the task manager, noting the pids for `Browser`,`GPU Process`, and `Tab: Meet`.
> these specific pids are only listed in the ChromeOS task manager by name and are difficult to discern what is what using `top`. 

`3` swtich over to console, `ctrl` + `alt` + `F2`, and with `top`, note the pids for `cras`,`cros_camera_algo`, and `cros_camera_service`.<br><br>


let's say all 6 pids noted are `7065`,`7074`,`1648`,`4408`,`4448`, and `22136`. we will taskset each pid individually then output to a file.
<br><br>

`4` taskset each pid one-by-one, remembering to add `-a` option to include all of the threads.
> taskset cannot change CPU affinity for a range of pids in one command, but something like this can work:
```
taskset -acp 8-11 7065 && \
taskset -acp 8-11 7074 && \
taskset -acp 8-11 1648 && \
taskset -acp 8-11 4408 && \
taskset -acp 8-11 4448 && \
taskset -acp 8-11 22136
```

`5` finally, outputting to file can be done by removing the CPU set:
```
taskset -acp 7065 | tee -a myfile.txt && \
taskset -acp 7074 | tee -a myfile.txt && \
taskset -acp 1648 | tee -a myfile.txt && \
taskset -acp 4408 | tee -a myfile.txt && \
taskset -acp 4448 | tee -a myfile.txt && \
taskset -acp 22136 | tee -a myfile.txt
```

done!


