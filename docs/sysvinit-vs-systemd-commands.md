##### sysvinit vs systemd commands
---

#### <i>Runlevels</i>
| sysvinit | systemd | #  |
| :------- | :------ | :- |
| telinit 0 \|\| poweroff \|\| shutdown now | systemctl poweroff | # shutdown computer |
| telinit 6 \|\| reboot \|\| poweroff --reboot | systemctl reboot | # halt system |
| halt \|\| poweroff --halt | systemctl halt | # halt system |


#### <i>Services</i>
| sysvinit | systemd | #  |
| :------- | :------ | :- |
| service dummy start | systemctl start dummy | # start service ( current session only ) |
| service dummy stop | systemctl stop dummy | # stop service ( current session only ) |
| service dummy restart | systemctl restart dummy | # restart service | 
| service dummy status | systemctl status dummy | # restart service |   
| service dummy on | systemctl enable dummy | # enable service across reboots |  
| service dummy off | systemctl disable dummy | # disable service across reboots |
| chkconfig dummy | systemctl is-enabled dummy | # checks to see if enabled on reboot |
| ls /etc/rc.d/init.d/ | ls /lib/systemd/system/*.service /etc/systemd/system/*.service | # list system services |
| chkconfig dummy --add | systemctl daemon-reload | # reload service or services cache |
| chkconfig --list | systemctl list-unit-files --type=service | # print runlevel services |
| chkconfig --list | ls /etc/systemd/system/*.wants/ | # print list of runlevel services |
| chkconfig dummy --list | ls /etc/systemd/system/*.wants/frobozz.service | # list runlevel for service |
| chkconfig --list \| grep 5:on | systemctl list-dependencies graphical.target | # list services for runlevel |


