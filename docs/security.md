![Alt text](https://images.pexels.com/photos/1148820/pexels-photo-1148820.jpeg?cs=srgb&dl=pexels-panumas-nikhomkhai-1148820.jpg&fm=jpg "a title")

#### limiting resources to users
---
Resource limits for the system are set by the kernel and are listed with `cat /proc/1/limits`. 
```
ulimit --help                               # show help menu for ulimit
ulimit -Ha                                  # show all hard limits for user
ulimit -Sa                                  # show all soft limits for user

ulimit -Hn                                  # displays the hard maximum size of files, defaults using -f
ulimit -Sn                                  # display current soft number
ulimit -Sn hard                             # soft limit can be set higher up to hard limit
ulimit -Sn                                  # display resulting soft number

```

#### limiting resources for others
---
```
su bob -c "ulimit -Sn"                      # as root
sudo -u bob -c "ulimit -Sn"                 # as superuser
```

#### permanently limiting resources for users
---
Editing `/etc/security/limits.conf` or `/etc/security/limits.d/90-nproc.conf` for RedHat Linux, default limits can be set for each user or groups.
```
nano /etc/security/limits.conf              # as root or superuser

```
For example, user rix, both soft and hard limits are set for how many processes can be executed. The `%` character denotes a group, which in this example limits all users for this group to only 3 users logged in to the system at any time. Finally, the `*` character sets a hard file size limit to `-1`, or unlimited.
```
<domain> <type> <item>    <value>
rix      soft   nproc     1024
rix      hard   nproc     1048576  
%guests  -      maxlogins 3
*        hard   fsize     -1

```
---






