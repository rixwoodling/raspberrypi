### Setting Up Network Interface File

<i>This step completes the wireless setup by properly configuring the network interface.</i>  
<sup>```?```<i> http://unix.stackexchange.com/questions/110757/locale-not-found-setting-locale-failed-what-should-i-do</i></sup>  
<sup>```?```<i> https://rohankapoor.com/2012/04/americanizing-the-raspberry-pi/</i></sup>  

---
###### The Quick Version
Copy and replace the following configuration to `/usr/local/etc/network/interfaces` (if not done already).    
```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
```
<sup>```!```<i> Change the 'interfaces' file located in `/usr/local/etc/network/interfaces` rather than in `/etc/network/interfaces` which is now a symbolic link.</i></sup>   

---
###### The Long Version
<sub><i>This is the default `interfaces` configuration file located in `/etc/network/`.</i></sub>  
```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
```
`1` List all available network interfaces. `ip link show`  
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT 
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP mode DEFAULT qlen 1000
    link/ether b8:27:eb:f4:14:71 brd ff:ff:ff:ff:ff:ff
3: wlan0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT qlen 1000
    link/ether 78:54:2e:ea:0a:4e brd ff:ff:ff:ff:ff:ff
```
><sup><i>The output shows:</i></sup>  
><sup><i>1. lo – Loopback interface.</i></sup>  
><sup><i>2. eth0 – First ethernet network interface.</i></sup>  
><sup><i>3. wlan0 – First wireless network interface.</i></sup>  

For eth0 with dhcp:
```
# The loopback network interface
auto lo eth0
iface lo inet loopback

# The primary network interface
iface eth0 inet dhcp
```
For eth0 static:
```
# The loopback network interface
auto lo eth0
iface lo inet loopback

# The primary network interface
iface eth0 inet static
    address 192.168.10.33
    netmask 255.255.255.0
    broadcast 192.168.10.255
    network 192.168.10.0
    gateway 192.168.10.254 
dns-nameservers 192.168.10.254
```
<sup><i>Reference: http://askubuntu.com/questions/214170/whats-the-default-etc-network-interfaces</i></sup>  
 
> <i><sup>On a Linux machine, the device name <b>lo</b> or the local loop is linked with the internal 127.0.0.1 address. The computer will have a hard time making your applications work if this device is not present; it is always there, even on computers which are not networked.</i></sup> 

> <i><sup>The first ethernet device, <b>eth0</b> in the case of a standard network interface card, points to your local LAN IP address. Normal client machines only have one network interface card. Routers, connecting networks together, have one network device for each network they serve.</i></sup>  
<sup><i>References: http://www.tldp.org/LDP/intro-linux/html/sect_10_02.html</i></sup>

- ifconfig: Enable your wireless device.
- iwlist: List the available wireless access points.
- iwconfig: Configure your wireless connection.
- dhclient: Get your IP Address via dhcp.
- wpa_supplicant: For use with WPA authentication.  
<sup><i>Reference: https://www.linux.com/learn/tutorials/374514-control-wireless-on-the-linux-desktop-with-these-tools</i></sup>  
`apt-get install wireless-tools`
