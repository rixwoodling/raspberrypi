##### Setting up NFS and mounting external hard drive

```
sudo nfsd restart #macos  
sudo service nfs-kernel-server restart #rpi  
  
apt-get install nfs-kernel-server  
  
nano /etc/exports  
/mnt/nfs/ *(rw,sync)  
  
showmount -e  
service nfs-kernel-server status  
rpcinfo -p | grep portmap  
cat /proc/filesystems | grep nfs  
  
/etc/init.d/nfs-kernel-server restart  
exportfs -ra  
service rpcbind restart  
reboot  
``` 

if:
```
rpc mount export: RPC: Unable to receive; errno = Connection refused
```
service rpcbind restart

