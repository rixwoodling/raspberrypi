###### Git Pull Dotfiles to Home Directory
---

<i>Initialize git in home directory, then pull git from remote path into home directory.</i>   
`git init`  
`git add .`  
`git`   

`cd`  
`rm .bash_logout .bashrc .profile`  
`git init`  
`git remote add origin https://path/to/dotfiles.git`  
`git pull origin master`  

