## SSH Key Authorization

<i>harden login security by allowing only authorized users and disallowing all 
password entries between two</i>  

---

create private and public keys on host computer. 
```
ssh-keygen -t rsa
```
><i>When prompted to "enter file in which to save the key", press 'enter'.  
Enter an optional passphrase when prompted for an extra layer of security.  
Both the private key 'id_rsa' and the public key 'id_rsa.pub' 
are located in ~/.ssh</i>

copy the public key, `id_rsa.pub`, to the remote system.  
```
ssh $USER@192.168.X.XXX "mkdir -p ~/.ssh && chmod 700 ~/.ssh"  
cat ~/.ssh/id_rsa.pub | ssh $USER@192.168.X.XXX "cat > ~/.ssh/authorized_keys"
```  
    
    Alternatively > ssh-copy-id -i ~/.ssh/id_rsa.pub user@198.168.X.XXX
    Change 'user' to login username and Xs to correct ip numbers.

Disable all password entries (root level). 
$   nano /etc/ssh/sshd_config 

    change the following to 'no':
```
ChallengeResponseAuthentication no
PasswordAuthentication no
UsePAM no
PermitRootLogin no
```
    Log out and log in with key-authorized computer. 
    Try to login with a different computer and ensure login is blocked.

---

##### remove authentication

`1` update sshd_config `nano /etc/ssh/sshd_config`  
```
PubkeyAuthorization no
PasswordAuthentication yes
PermitRootLogin yes
```
`2` restart ssh `/etc/init.d/ssh restart`

---


Resources
?   https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2
?   https://www.cyberciti.biz/faq/how-to-disable-ssh-password-login-on-linux/
?   https://superuser.com/questions/303358/why-is-ssh-key-authentication-better-than-password-authentication
?   http://stackoverflow.com/questions/15802179/list-of-all-users-that-can-connect-via-ssh
?   https://www.howtogeek.com/howto/linux/security-tip-disable-root-ssh-login-on-linux/
?   https://unix.stackexchange.com/questions/36540/why-am-i-still-getting-a-password-prompt-with-ssh-with-public-key-authentication
?   https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md
?   https://www.howtoforge.com/set-up-ssh-with-public-key-authentication-debian-etch
?   https://www.howtoforge.com/linux-basics-how-to-install-ssh-keys-on-the-shell
?   https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server
?   https://plusbryan.com/my-first-5-minutes-on-a-server-or-essential-security-for-linux-servers
