### building go

to install
```
wget https://go.dev/dl/go1.17.5.linux-amd64.tar.gz
tar -C ~/ -xzf go1.17.5.linux-amd64.tar.gz
export PATH=$PATH:/$HOME/go/bin
go version
```

to remove
```
chmod 700 -R ~/go
rm -rf ~/go
```
