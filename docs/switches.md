#### managed switches

<p><i>
unlike an unmanaged switch that automatically allows devices on a wired network to communicate, a managed switch like the Cisco SG350-10 can [shut down ports](#ports), [limit access to networks](#access), separating devices on the network and control who has access to what device, review the data statistics of each port, disable unused ports, limit access to your network, limit speed, prioritize traffic, adjust parameters, and more.  
</i></p>
<p><i>
because a network switch operates on the data link layer, or OSI layer 2, connections are created via MAC addresses and are not connected via ssh, which uses a layer 3 network. There are a few ways to connect to a managed switch.
</i></p>

---
###### via console
<i>with a console cable attached to the back of the switch, open the console.</i>
```
ls /dev/tty.*
screen /dev/tty.usbserial-AB0K45UJ
```

###### via telnet
<i>with a console cable attached to the back of the switch, open the console.</i>
```

```

###### via web-browser
<i>with a console cable attached to the back of the switch, open the console.</i>
```

```

---
###### <a name="shut-port"></a> shutting down a port

```
screen /dev/tty.usbserial-AB0K45UJ 115200
```

```
Switch> enable
Switch#
```

Log into the switch via command line

SG350-10
```
?
```
```
show bootvar
```
```
show interface status
```
```
show power
```

###### <a name="ports"></a> shutting down ports
###### <a name="access"></a> limiting access to networks
###### <a name="shut-port"></a> shutting down a port
###### <a name="shut-port"></a> shutting down a port

```
Sorry, could not find a PTY.  
Cannot open line '/dev/tty.usbserial-AB0K45UJ' for R/W: Resource busy
# turn off and on the switch 
```

```
switch20724f#04-Nov-2018 08:20:14 %LINK-W-Down:  gi5
04-Nov-2018 08:20:16 %LINK-I-Up:  gi5
04-Nov-2018 08:20:21 %STP-W-PORTSTATUS: gi5: STP status Forwarding
04-Nov-2018 08:21:12 %LINK-W-Down:  gi5
04-Nov-2018 08:21:14 %LINK-I-Up:  gi5
04-Nov-2018 08:21:19 %STP-W-PORTSTATUS: gi5: STP status Forwarding
04-Nov-2018 08:21:53 %BOOTP_DHCP_CL-W-DHCPIPCANDIDATE: The device is waiting for IP address verification on interface Vlan 1 , IP 192.168.1.101, mask 255.255.255.0, DHCP server 192.168.1.1 
04-Nov-2018 08:21:55 %BOOTP_DHCP_CL-I-DHCPCONFIGURED: The device has been configured on interface Vlan 1 , IP 192.168.1.101, mask 255.255.255.0, DHCP server 192.168.1.1 
04-Nov-2018 08:22:30 %LINK-W-Down:  gi5
04-Nov-2018 08:22:33 %LINK-I-Up:  gi5
04-Nov-2018 08:22:37 %STP-W-PORTSTATUS: gi5: STP status Forwarding
04-Nov-2018 08:22:43 %LINK-W-Down:  gi5
04-Nov-2018 08:22:46 %LINK-I-Up:  gi5
04-Nov-2018 08:22:50 %STP-W-PORTSTATUS: gi5: STP status Forwarding
04-Nov-2018 08:22:59 %BOOTP_DHCP_CL-W-DHCPIPCANDIDATE: The device is waiting for IP address verification on interface Vlan 1 , IP 192.168.1.101, mask 255.255.255.0, DHCP server 192.168.1.1 
04-Nov-2018 08:23:01 %BOOTP_DHCP_CL-I-DHCPCONFIGURED: The device has been configured on interface Vlan 1 , IP 192.168.1.101, mask 255.255.255.0, DHCP server 192.168.1.1 
04-Nov-2018 08:23:56 %DHCPV6CLIENT-I-STATELESSDATA: DHCP Stateless information received on vlan 1 from DHCP Server fe80::5aef:68ff:fe82:3f9a was updated
04-Nov-2018 08:24:05 %DHCPV6CLIENT-I-ADDR: DHCPv6 Address 2601:1c0:5202:87fb::4 received on vlan 1 from DHCP Server fe80::5aef:68ff:fe82:3f9a was added
04-Nov-2018 08:24:06 %DHCPV6CLIENT-I-ADDR: DHCPv6 Address 2601:1c0:5202:87fb::4 received on vlan 1 from DHCP Server fe80::5aef:68ff:fe82:3f9a was validated
```



blinking green status led<br>
reboot router<br>
`ctrl + a` `k` to kill the screen window


