###### nethack commands
---

```
Hello rix, the human Monk, welcome back to NetHack!

                          -----#
      ------------     ###....|#              ##
      |.......@..f######  |^...#   ------------#       -----
      |..........|       #....|#   |...........##   ###...^|
      |..........|       #|.^.|####...........|#####   |...|
      --.---------       #-----### |..........|#   ####|....####
        ##               #       # |......>...|#      #-...|####### #####
         #####           #       ##............#      #-----#  #  ------.---
             ###         #         --------.---##############  ###|........|
               ###       #                 #   #       #   ###    |........|
                 #       #                     #       #         #-........|
                 ####    #                   ###       #     #####----------
          ----------.----#                   #################
          |.............-#                   #---.-----#
          |.............|                    #|.......|#
          |..............## ##################........|#
          |.............|                     |.....^.|#
          ---------------                     ---------


Rix the Novice                St:18/02 Dx:13 Co:10 In:11 Wi:13 Ch:8 Neutral
Dlvl:1   $:7 HP:30(30) Pw:19(19) AC:4 Xp:4  
```

```
S                           # save and exit
O                           # options menu
< >                         # back and next page
esc                         # escape key gets out of menus and prompts                  
ctrl + p                    # show last onscreen message              
h j k l                     # left down up right
y u b n                     # nw ne sw se
       
;         far look          # after ; nav with hjkl to square and ; again to identify
:         near look         # identify what is on current square
a         apply item        # 
d         drop              # from inventory or to relocate a chest
e         eat               # eat comestible or corpse for hunger but not for HP
i         inventory         # current inventory list 
n         number of times   # n20 followed by s, for example, will search 20 times
p         pay               # pay shop seller if acquired anything to leave
q         quaff             # drink water at a fountain, or drink a potion
r         read              # read item from inventory
s         search            # if at a dead end, search
t         throw             # throw food to pets or throw weapons like shurikens
-
E         engrave           # engrave with fingers or wands
I         inventory         # show Coins, Weapons, Armor, Potions, Tools, etc
T         take off          # remove armor attire before upgrading with 'W'      
W         wear armor        # wear armor attire with 'W' key
^         inspect trap      # inspect trap trap with ^ key 


```
```
COMMANDS
#chat                       # chat with pets or peaceful entiities
#engrave                    # engrave writing on floor, such as Elbereth in mines
#kick                       # kick, useful for kicking down doors 
#loot                       # loot chests but also to add things to these containers
#pray                       # pray to the gods for help


```
---
```
DUNGEON
{ }       fountains         # quaff the water with the 'q' key to drink
> <       stairs            # stairs up < and stairs down >
_         altar             # offer corpses and #pray to receive gifts from the gods
#         sink              # kick a sink to verify but usually in a room
+         door              # usually locked but can be kicked down
^         traps             # most cause harm and some can be hiding behind items

```
```
TRAPS
^         arrow trap        # chance for arrows but can be disarmed with #untrap
^         rolling boulder   # when triggered it may or may not have a boulder
^         trap door         # including pets and monsters, will fall into next level


```
```
ITEMS
(         tool              # a container type holding a few items, open with 'a' apply
)         weapon            # can throw weapons with 't' key for throw
?         scroll            #
!         potion            # random colors with unidentified effects 
=         ring              # 
*         gem               # 
+         spellbook         # 
/         wand              #
$         gold              # gold pieces
(         large box         # a container type holding a few items, open with 'a' apply
(         shuriken          # can throw weapons with 't' key for throw

```
```
ARMOR
[         banded mail       # use 'w' wear key after picking up with `,` key


```
```
MONSTERS
k         kobold            # a kobold is one of the easiest low-level monsters, but poisonous
d         jackel            #
F         lichen            # easy to kill monster that sticky attacks but gives no damage
x         grid bug          # weak zapping but that barely delivers an attack
:         newt              # eating corpse of newt will give power increase
r         sewer rat         # stronger than a newt but weaker than a kobold
B         bat               # weak monster but eating causes being stunned
n         nymph             # water nymphs appear around water
h         hobbit            # chat with hobbits before attacking as some are peaceful
o         goblin            # easy monster that can be killed and sacrificed
Z         zombie            # zombie, ghoul or skeleton drop tained zombies unfit to eat
G         gnomes            # gnomes and lord gnomes
M         mummy             # similar to zombies but can drop wrapping, or invis cloaks
Y         monkeys           # the weakest of apes will still steal from you
i         imp               # minor demon should flee with an Elbereth engraving
a         giant ant         # weak enough but can be numerous

```
```
PETS
f         feline            # cat
d         little dog        # 

```
```
TIPS


```

