---
##### creating a multiboot usb



`sudo parted -s /dev/sdb mklabel msdos`  

<i>if an error occurs
```
Error: Error informing the kernel about modifications to partition /dev/sdb1 -- Device or resource busy. 
This means Linux won't know about any changes you made to /dev/sdb1 until you reboot -- so you shouldn't 
mount it or use it in any way before rebooting.
Error: Failed to add partition 1 (Device of resource busy)
```

```
grub-install --force --removable --no-floppy --target=x86_64-efi --boot-directory=/mnt/USB/boot --efi-directory=/mnt/USB
```
```
grub-install --force --removable --no-floppy --target=i386-efi --boot-directory=/mnt/USB/boot --efi-directory=/mnt/USB
```
```
grub-install --force --removable --no-floppy --target=i386-pc --boot-directory=/mnt/USB/boot /dev/sdx
```

```
# Installing legacy grub2
sudo grub2-install --target=i386-pc --recheck --boot-directory="/media/data/boot" /dev/sdb
# Installing grub for efi
sudo grub2-install --target=x86_64-efi --recheck --removable --efi-directory="/media/efi" --boot-directory="/media/data/boot"  
```

https://linuxconfig.org/how-to-create-multiboot-usb-with-linux
