`WHEEZY DEPRECIATED`

### Configure an Advanced Wireless Network

<sup><i>
A wireless Wi-Fi dongle won't work unless wireless tools are installed to 
this barebones version of Linux.</i>  
<sup>These are more advanced network configurations. If a more minimal configuration is needed, go [here](https://github.com/rixwoodling/linux/blob/master/tmp/minimal-network.md).</sup> 

---
###### Multiple Wi-Fi Configuration

<sub><i>This network configuration can connect to multiple access points, say, between home and work.  
This configuration adds wireless capabilities on top of the default configuration, namely `wlan0`, still relying on a dynamic ip. The local wifi username/password will be created in a newly created `wpa-roam.conf` configuration file. Hot-plugging is added for convenience.</i></sub>  

<sup>`1` Backup the `interfaces` file. `cp /etc/network/interfaces /etc/network/interfaces.bak`</sup>  
<sup>`2` Verify `ip` and `ping` are installed. `dpkg -s iputils-ping iproute | egrep "Package|Status|Version”`.</sup>  
<sup>`3` Install `iw` and `wpa_supplicant`. `apt-get install -y iw wpa_supplicant`</sup>  
<sup>`4` Copy and Paste the interfaces file with the configuration below. `nano /etc/network/interfaces`</sup>  

```
# /etc/network/interfaces

auto lo
iface lo inet loopback

auto eth0
allow-hotplug eth0
iface eth0 inet dhcp

auto wlan0
allow-hotplug wlan0
iface wlan0 inet manual
wpa-roam /etc/wpa_supplicant/wpa-roam.conf
  iface home inet dhcp
  iface default inet dhcp
```
<sub>`5` Create a wpa configuration file with the info below. `nano /etc/wpa_supplicant/wpa-roam.conf`</sub>  

```
# /etc/wpa_supplicant/wpa-roam.conf

update_config=1
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev

network={
        ssid="********"
        psk="********"
        id_str="home"
}
```
<sup>`!`<i> Replace `********` with WIFI username and password. Both the username and password must be set in quotes.</i></sup>   

---
###### Make Interfaces/WPA-Supplicant File Accessible 

<sub><i>By making the interfaces and other network files accessible in the boot folder, these files can then be viewed and edited by another computer with SD card access. Should something go wrong with accessing the Raspberry Pi via the network, such as via <i>ssh</i>, the network files can easily be reverted back to a working script. This will aid in testing various configurations without needing to reinstall the system if a network configuration doesn't work.</i></sub>  

<sub>`1` Move `interfaces` file to `/boot` and create a symbolic link back to `/etc/network`.</sub>  
```
cp /etc/network/interfaces /boot/network/interfaces
cp /boot/network/interfaces /etc/network/interfaces.bak
rm -r /etc/network/interfaces
ln -s /boot/network/interfaces /etc/network/interfaces
```
<sub>`2` Move `wpa-roam.conf` file to `/boot` and create a symbolic link back to `/etc/wpa_supplicant`.</sub>  
```
cp /etc/wpa_supplicant/wpa-roam.conf /boot/network/wpa-roam.conf
cp /boot/network/wpa-roam.conf /etc/network/wpa-roam.conf.bak
rm -r /etc/wpa_supplicant/wpa-roam.conf
ln -s /boot/network/wpa-roam.conf /etc/wpa_supplicant/wpa-roam.conf
```
<sub>`!`<i> A potential security risk is created by having the WEP passwords exposed in the `/boot` partition of the SD card. This should only be used for testing purposes.</i></sub>      

---
<sup>Roaming Network References</sup>
<sup><i>[•](http://manual.aptosid.com/en/inet-setup-en.htm)</i></sup>
<sup><i>[•](http://www.blackmoreops.com/2014/09/18/connect-to-wifi-network-from-command-line-in-linux/)</i></sup>
<sup><i>[•](http://raspberrypi.stackexchange.com/questions/9257/whats-the-difference-between-wpa-roam-and-wpa-conf-in-the-etc-network-inte)</i></sup>
<sup><i>[•](https://www.debuntu.org/how-to-wifi-roaming-with-wpa-supplicant/)</i></sup>
<sup><i>[•](https://rpi.tnet.com/project/faqs/headlessportablewifi)</i></sup>
<sup>Grep References</sup>
<sup><i>[•](http://www.thegeekstuff.com/2011/10/grep-or-and-not-operators/)</i></sup>
<sup><i>[•](http://stackoverflow.com/questions/3070141/how-do-i-use-grep-to-extract-a-specific-field-value-from-lines)</i></sup>
<sup>Network Accessability References</sup>
<sup><i>[•](https://rpi.tnet.com/project/faqs/winaccessinterfacesfile)</i></sup>
