`WHEEZY DEPRECIATED`

##### Configure an Advanced Wireless Network

<sup><i>
A wireless Wi-Fi dongle won't work unless wireless tools are installed 
to this barebones version of Linux.
</i>  
These are more advanced network configurations. If a more minimal 
configuration is needed, go [here]
(https://github.com/rixwoodling/linux/blob/master/tmp/minimal-network.md).
</sup> 

---
###### Simple Wireless Network Configuration

<sub><i>
This is a near-minimal network configuration that adds to the default network 
configuration. This configuration adds wireless, namely `wlan0`, still relying 
on a dynamic ip. The local wifi username/password is also set here. Lastly, 
hot-plugging is optionally added for convenience.
</i></sub>  

<sup><i>
[Configure a Simple Wireless Network Configuration...]
(https://github.com/rixwoodling/linux/blob/master/tmp/network.wpa-roam.md)
</i></sup>   

---
###### Multiple Wi-Fi Configuration

<sub><i>
This network configuration can connect to multiple access points, say, between 
home and work. This configuration adds wireless capabilities on top of the 
default configuration, namely `wlan0`, still relying on a dynamic ip. 
The local wifi username/password will be created in a newly created 
`wpa-roam.conf` configuration file. Hot-plugging is added for convenience.
</i></sub>  

<sup><i>
[Configure a Multiple Wi-Fi Configuration...]
(https://github.com/rixwoodling/linux/blob/master/tmp/network.wpa-roam.md)
</i></sup>   

---