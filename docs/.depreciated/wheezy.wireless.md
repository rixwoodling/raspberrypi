`WHEEZY DEPRECIATED`

### Install Wireless Tools

<i>A wireless Wi-Fi dongle won't work unless wireless tools are installed to this barebones version of Linux.</i>  
<sup>`?` <i>https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md</i></sup>  
<sup>```?```<i> http://www.cyberciti.biz/faq/debian-linux-wpa-wpa2-wireless-wifi-networking/</i></sup>  
<sup>```?```<i> https://www.raspberrypi.org/forums/viewtopic.php?f=28&t=44044</i></sup>  

---

`1` Install wireless tools. 
```
apt-get install rfkill zd1211-firmware hostapd hostap-utils iw dnsmasq bridge-utils
```
><sup>```?```<i> rfkill - a tool for enabling and disabling wireless devices [Info](https://wireless.wiki.kernel.org/en/users/documentation/rfkill)</i></sup>  
><sup>```?```<i> zd1211-firmware - firmware support for USB-wireless devices [Device list](https://wireless.wiki.kernel.org/en/users/Drivers/zd1211rw/devices) | [Info](http://www.linuxwireless.org/en/users/Drivers/zd1211rw/)</i></sup>  
><sup>```?```<i> hostapd - a user space daemon for access point and authentication servers [Info](https://nims11.wordpress.com/2012/04/27/hostapd-the-linux-way-to-create-virtual-wifi-access-point/)</i></sup>  
><sup>```?```<i> hostap-utils - a package containing a command line utility to control the IEEE 802.1x/WPA/EAP/RADIUS Authenticator. [Info](https://wiki.openwrt.org/doc/howto/wireless.utilities)</i></sup>  
><sup>```?```<i> iw - shows and manipulates wireless devices and their configurations [Info](https://sandilands.info/sgordon/capturing-wifi-in-monitor-mode-with-iw)</i></sup>  
><sup>```?```<i> dnsmasq - provides network infrastructure for small networks [Info](http://www.thekelleys.org.uk/dnsmasq/doc.html)</i></sup>  
><sup>```?```<i> bridge-utils - 	utilities for configuring the Linux ethernet bridge [Info](http://www.linuxfoundation.org/collaborate/workgroups/networking/bridge)</i></sup>  

`2` Install USB tools. ```apt-get install usbutils```  
><sup>```?```<i> lsusb  is  a  utility for displaying information about USB buses in the
       system and the devices connected to them.</i></sup>  

`3` Verify the Wi-Fi dongle is detected. `lsusb`  
```
Bus 001 Device 002: ID 0424:9512 Standard Microsystems Corp. 
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 001 Device 003: ID 0424:ec00 Standard Microsystems Corp. 
Bus 001 Device 004: ID 2001:3308 D-Link Corp. DWA-121 802.11n Wireless N 150 Pico Adapter [Realtek RTL8188CUS]
```

The router provides the service of being a home router, ssh, as well as any external requests for access to machines while away. 

```sudo apt-get update```
```sudo apt-get upgrade```

一 Install components:
   ```sudo apt-get install rfkill zd1211-firmware hostapd hostap-utils iw dnsmasq bridge-utils```

二 Verify if Wi-Fi dongle is recognized by Pi
```lsusb``` If this happens: 
> lsusb: command not found

type this: ```sudo apt-get install usbutils``` 
