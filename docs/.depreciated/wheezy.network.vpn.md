`WHEEZY DEPRECIATED`

### Configure a Virtual Private Network

<i>A wireless Wi-Fi dongle won't work unless wireless tools are installed to this barebones version of Linux.</i>  
<sup>These are more advanced network configurations. If a more minimal configuration is needed, go [here](https://github.com/rixwoodling/linux/blob/master/tmp/minimal-network.md).</sup> 

---
###### Install OpenVPN / Easy-RSA  

<sub>`1` Install `openvpn`. `sudo apt-get install -y openvpn`</sub>  
<sub>`2` Copy `easy-rsa` files into `/etc/openvpn/easy-rsa`.</sub> `cp –r /usr/share/doc/openvpn/examples/easy-rsa/2.0 /etc/openvpn/easy-rsa`  
><sup>Openvpn should come with easy-rsa. If not, `git clone https://github.com/OpenVPN/easy-rsa.git` and copy files to `/etc/openvpn/easy-rsa`. Then, `git checkout 2.2.2`. All is normal if `# HEAD is now at 19c3186... creating 2.x branch for tracking changes` is shown. Copy those files with `cp -r easy-rsa/2.0/ /etc/openvpn/easy-rsa`.</sup>  

###### Setting Up the Certificate  

<sup>`3` Edit `vars` file. `nano /etc/openvpn/easy-rsa/vars`</sup>

#change…
export EASY_RSA="`pwd`"
#...to
export EASY_RSA="/etc/openvpn/easy-rsa"

export KEY_SIZE=1024 
#change to…
export KEY_SIZE=2048

###### build our encryption certificates

source ./vars
./clean-all
./build-ca

######
./build-key-server “name_of_server”

`Common Name` must be set to your server name you entered above (for example “name_of_server”), it should do this by default but verify it to be safe.
`A challenge password` <i>must</i> be left blank! Don't enter anything here.
`Sign the certificate?`, enter `y` to sign it.
`1 out of 1 certificate requests certified, commit?`, again press `y` of course.

###### Creating Client Certificates

<sup>`3` Create a client key. `./build-key-pass client1`</sup>  
<sup>`-` `Enter PEM pass phrase`: Enter a password (you will remember). This will be used every time you connect to your server.</sup>  
<sup>`-` `A challenge password`: <i>Must</i> be left blank!</sup>  
<sup>`-` `Sign the certificate?`: Enter `y` to sign it for 10 years.</sup>  

###### Sign the Keys
cd keys  
openssl rsa -in User1.key -des3 -out User1.3des.key

`Enter pass phrase for Yoga.key`: Enter the same PEM pass phrase here.
`Enter PEM pass phrase`: Enter the same pass phrase again.
Now change directories back to `/etc/openvpn/easy-rsa/`:

cd ..
./build-dh

`openvpn --genkey --secret keys/ta.key` 

###### Configuring OpenVPN
`nano /etc/openvpn/server.conf`

`cat /etc/network/interfaces`
```
auto lo
iface lo inet loopback

auto eth0
allow-hotplug eth0
iface eth0 inet manual
    pre-up /etc/firewall-openvpn-rules.sh
    
auto wlan0
allow-hotplug wlan0
iface wlan0 inet manual
wpa-roam /etc/wpa_supplicant/wpa-roam.conf
    iface home inet dhcp
    iface default inet dhcp
```

---
<sup>VPN References</sup>
<sup><i>[•](http://en.code-bude.net/2013/07/21/how-to-setup-ddclient-on-raspberry-pi-for-use-with-namecheap-dyndns/)</i></sup>
<sup><i>[•](https://www.raspberrypi.org/forums/viewtopic.php?t=81657)</i></sup>
<sup><i>[•](https://www.digitalocean.com/community/tutorials/how-to-configure-and-connect-to-a-private-openvpn-server-on-freebsd-10-1)</i></sup>
<sup><i>[•](https://community.openvpn.net/openvpn/wiki/Easy_Windows_Guide#DownloadingandInstallingOpenVPN)</i></sup>
<sup><i>[•](https://www.youtube.com/watch?v=_4I8W-jpWg0)</i></sup>
<sup><i>[•](https://github.com/OpenVPN/openvpn/blob/master/sample/sample-config-files/server.conf)</i></sup>
<sup><i>[•](http://www.pontikis.net/blog/openvpn_on_debian_server)</i></sup>
<sup><i>[•](http://offthegrid.io/how-to-set-up-an-openvpn-server-on-a-raspberry-pi-2/)</i></sup>
<sup><i>[•](http://offthegrid.io/connecting-to-an-openvpn-server/)</i></sup>
<sup><i>[•](http://readwrite.com/2014/04/10/raspberry-pi-vpn-tutorial-server-secure-web-browsing/)</i></sup>
<sup><i>[•](http://readwrite.com/2014/04/11/building-a-raspberry-pi-vpn-part-two-creating-an-encrypted-client-side/#awesm=~oB89WBfWrt21bV)</i></sup>
<sup><i>[•](http://spielprinzip.com/blog/allgemein/installing-openvpn-and-connect-via-ssh-to-a-remote-machine/)</i></sup>
<sup><i>[•](https://forums.openvpn.net/topic12938.html)</i></sup>
<sup><i>[•](http://albanianwizard.org/tls-error-tls-key-negotiation-failed-to-occur-within-60-seconds.albanianwizard)</i></sup>

