`WHEEZY DEPRECIATED`

### Resize the Main Partition on the SD Card

<i>These steps will expand the main partition to use the entire space of the SD card.</i>  
<sup>```?```<i> https://minibianpi.wordpress.com/how-to/resize-sd/</i></sup> 

---

`1` Change the partition table with fdisk. ```fdisk /dev/mmcblk0```  
><sup>```p```<i> to see all SD card partitions.</i></sup>  
><sup>```d``` ```2```<i> to delete the main partition.</i></sup>  
><sup>```n``` ```p``` ```2```<i> to create a new primary partition.</i></sup>  
><sup>```enter``` to accept the default number of the first sector.</i></sup>  
><sup>```enter``` to accept the default number of the last sector.</i></sup>  
><sup>```w```<i> to write the new partition table.</i></sup>  

<sup>```!```<i> The error `The partition table has been altered!` will appear. Ignore and proceed with Steps 2 and 3.</i></sup>    

`2` Reboot the RPi. ```reboot```  

`3` Resize the filesystem on the partition. ```resize2fs /dev/mmcblk0p2```  
><sup>```df -h```<i> to check the new partition size.</i></sup>  
