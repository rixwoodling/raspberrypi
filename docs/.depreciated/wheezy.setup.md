`WHEEZY DEPRECIATED`

<img src="https://github.com/rixwoodling/linux/blob/master/tmp/art/raspberrypi.png" height="50" />
<img src="https://github.com/rixwoodling/linux/blob/master/tmp/art/linux.png" height="50" />
<img src="https://github.com/rixwoodling/linux/blob/master/tmp/art/debian.png" height="50" />

#### Installation and Set-Up  
<sup>
`+` Install Linux on an SD card with a host system. [Mac](https://github.com/rixwoodling/linux/blob/master/tmp/installation.mac.md) | [Linux](https://github.com/rixwoodling/linux/blob/master/tmp/installation.md) | [Latest Release](https://github.com/rixwoodling/linux/releases)  
`+` [SSH](https://github.com/rixwoodling/linux/blob/master/tmp/ssh.md) into the Raspberry Pi. `ssh root@192.168.X.XXX`.  
`+` [Expand](https://github.com/rixwoodling/linux/blob/master/tmp/resize.md) main partition with `fdisk`.   
`+` Update and upgrade. `apt-get update -y && apt-get upgrade -y && apt-get clean`  
</sup>

#### Initial Configurations  
<sup>
`+` [Rotate](https://github.com/rixwoodling/linux/blob/master/tmp/rotatelog.md) logs. `/var/log/dpkg.log` `/var/log/apt/history.log`  
`+` Install Nano. `apt-get install -y nano`  
`+` Set [locale](./locale.md) and [localtime](https://github.com/rixwoodling/linux/blob/master/tmp/localtime.md).  
`+` Configure the wireless [network](https://github.com/rixwoodling/linux/blob/master/tmp/network.md).  
`+`  Update and upgrade. `apt-get update -y && apt-get upgrade -y && apt-get clean`    
</sup>

#### Essential Security  
<sup>
`一` Set up a strong root password. `passwd`   
`二` Update and upgrade. `apt-get update && apt-get upgrade`  
`三` Install fail2ban. `apt-get install fail2ban`  
</sup>
