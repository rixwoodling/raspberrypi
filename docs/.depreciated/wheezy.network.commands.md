`WHEEZY DEPRECIATED`

```
net-tools           iproute2                iw

arp -na             ip neigh
ifconfig            ip link
ifconfig -a         ip addr show
ifconfig --help     ip help
ifconfig -s         ip -s link
ifconfig eth0 up    ip link set eth0 up
ipmaddr             ip maddr
iptunnel            ip tunnel
netstat             ss
netstat -i          ip -s link
netstat -g          ip maddr
netstat -l          ss -l
netstat -r          ip route
route add           ip route add
route del           ip route del
route -n            ip route show
vconfig             ip link

```
