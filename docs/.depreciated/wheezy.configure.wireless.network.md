`WHEEZY DEPRECIATED`

### Configure Wireless Network

<i>A wireless Wi-Fi dongle won't work unless wireless tools are installed to this barebones version of Linux.</i>  
<sup><i>Configure the network by using one of the configurations below. If more advanced configurations are needed, go [here](https://github.com/rixwoodling/linux/blob/master/tmp/advanced-networks.md).</i></sup> 

---
###### Default Network Configuration  
<sub><i>This is the default network configuration located in `/etc/network/interfaces`. This configuration offers the always present loopback and a simple ethernet config with dhcp, which automatically assigns a next-in-line local ip. No action is required to modify the default configuration for basic ethernet connection.</i></sub>  

```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
```
<sup>`!`<i> With this configuration, ethernet must be plugged in if working on a headless system. Hotplugging isn't supported either.</i></sup>   

---
###### Simple Wireless Network Configuration

<sub><i>This is a near-minimal network configuration that adds to the default network configuration. This configuration adds wireless, namely `wlan0`, still relying on a dynamic ip. The local wifi username/password is also set here. Lastly, hot-plugging is optionally added for convenience.</i></sub>  

<sup>`1` Backup the `interfaces` file. `cp /etc/network/interfaces /etc/network/interfaces.bak`</sup>  
<sup>`2` Verify `ip` and `ping` are installed. `dpkg -s iputils-ping iproute | egrep "Package|Status|Version”`.</sup>  
<sup>`3` Install `iw` and `wpa_supplicant`. `apt-get install -y iw wpa_supplicant`</sup>  
<sup>`4` Copy and Paste the interfaces file with the configuration below. `nano /etc/network/interfaces`</sup>  

```
# /etc/network/interfaces

auto lo
iface lo inet loopback

auto eth0
allow-hotplug eth0
iface eth0 inet dhcp

auto wlan0
allow-hotplug wlan0
iface wlan0 inet dhcp
wpa-ssid "********"
wpa-psk "********"
```
<sup>`!`<i> Replace `********` with WIFI username and password. Both the username and password must be set in quotes.</i></sup>   

---
<sup><i> [More Advanced Wireless Configurations...](https://github.com/rixwoodling/linux/blob/master/tmp/advanced-networks.md)</i></sup>   
