`WHEEZY DEPRECIATED`

### Setting Locales and Timezones

<i>This is a tutorial on how to connect to the Raspberry Pi from either a Mac or Linux using SSH.</i>  
<sup>```?```<i> http://unix.stackexchange.com/questions/110757/locale-not-found-setting-locale-failed-what-should-i-do</i></sup>  
<sup>```?```<i> https://rohankapoor.com/2012/04/americanizing-the-raspberry-pi/</i></sup>  

---

`1` Install wireless tools. 
```
apt-get install rfkill zd1211-firmware hostapd hostap-utils iw dnsmasq bridge-utils
```
<sup>```?```<i> These packages will communicate with the wifi dongle.</i></sup>    
<sup>```?```<i> [More wireless networking tools...](http://github.com/rixwoodling/linux/tmp/)</i></sup>    

---
`2` Update network configuration file.  
<sub><i>Copy and replace the following configuration to `/usr/local/etc/network/interfaces` (if not done already).</i></sub>   
```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
```
<sup>```!```<i> Change the 'interfaces' file located in `/usr/local/etc/network/interfaces` rather than in `/etc/network/interfaces` which is now a symbolic link.</i></sup>   
<sup>```?```<i> [More network interface customizations...](http://github.com/rixwoodling/linux/tmp/)</i></sup>    
