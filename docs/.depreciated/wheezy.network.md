`WHEEZY DEPRECIATED`

### Configure Wireless Network

<i>A wireless Wi-Fi dongle won't work unless wireless tools are installed to this barebones version of Linux.</i>  
<sup><i>Configure the network by using one of the configurations below. If more advanced configurations are needed, go [here](https://github.com/rixwoodling/linux/blob/master/tmp/advanced-networks.md).</i></sup> 

---
###### Default Network Configuration  
<sub><i>This is the default network configuration located in `/etc/network/interfaces`. This configuration offers the always present loopback and a simple ethernet config with dhcp, which automatically assigns a next-in-line local ip. No action is required to modify the default configuration for basic ethernet connection.</i></sub>  

```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
```
<sup>`!`<i> With this configuration, ethernet must be plugged in if working on a headless system. Hotplugging isn't supported either.</i></sup>   

---
###### Simple Wireless Network Configuration

<sub><i>This is a near-minimal network configuration that adds to the default network configuration. This configuration adds wireless, namely `wlan0`, still relying on a dynamic ip. The local wifi username/password is also set here. Lastly, hot-plugging is optionally added for convenience.</i></sub>  

<sup><i>[Configure a Simple Wireless Network Configuration...](https://github.com/rixwoodling/linux/blob/master/tmp/network.wpa-roam.md)</i></sup>   

---
###### Multiple Wi-Fi Configuration

<sub><i>This network configuration can connect to multiple access points, say, between home and work.  
This configuration adds wireless capabilities on top of the default configuration, namely `wlan0`, still relying on a dynamic ip. The local wifi username/password will be created in a newly created `wpa-roam.conf` configuration file. Hot-plugging is added for convenience.</i></sub>  

<sup><i>[Configure a Multiple Wi-Fi Configuration...](https://github.com/rixwoodling/linux/blob/master/tmp/network.wpa-roam.md)</i></sup>   

---
###### Virtual Private Network Configuration

<sup><i>[Configure a Virtual Private Network...](https://github.com/rixwoodling/linux/blob/master/tmp/network.vpn.md)</i></sup>   
