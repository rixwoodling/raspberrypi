`WHEEZY DEPRECIATED`

### Configure Wireless Network

<i>A wireless Wi-Fi dongle won't work unless wireless tools are installed to this barebones version of Linux.</i>  

---
###### Install Wireless Tools
```
apt-get install rfkill zd1211-firmware hostapd hostap-utils iw dnsmasq bridge-utils
```
<sup>```?```<i> These packages will communicate with the wifi dongle.</i></sup>    
<sup>```?```<i> [More wireless networking tools...](http://github.com/rixwoodling/linux/tmp/)</i></sup>    

---
###### Default Network Configuration  
<sub><i>This is the default network configuration located in `/etc/network/interfaces`. This configuration offers the always present loopback and a simple ethernet config with dhcp, which automatically assigns a next-in-line local ip. No action is required to modify the default configuration for basic ethernet connection.</i></sub>  

```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
```
<sup>`!`<i> With this configuration, ethernet must be plugged in if working on a headless system. Hotplugging isn't supported either.</i></sup>   

---
###### Simple Wireless Network Configuration

><sub><i>This is a near-minimal network configuration that adds to the default network configuration. This configuration adds wireless, namely `wlan0`, still relying on a dynamic ip. The local wifi username/password is also set here. Lastly, hot-plugging is optionally added for convenience.</i></sub>  

<sup>`1` Backup the `interfaces` file. `cp /etc/network/interfaces /etc/network/interfaces.bak`</sup>  
<sup>`2` Verify `ip` and `ping` are installed. `dpkg -s iputils-ping iproute | egrep "Package|Status|Version”`.</sup>  
<sup>`3` Install `iw` and `wpa_supplicant`. `apt-get install -y iw wpa_supplicant`</sup>  
<sup>`4` Copy and Paste the interfaces file with the configuration below. `nano /etc/network/interfaces`</sup>  

```
# /etc/network/interfaces

auto lo
iface lo inet loopback

auto eth0
allow-hotplug eth0
iface eth0 inet dhcp

auto wlan0
allow-hotplug wlan0
iface wlan0 inet dhcp
wpa-ssid "********"
wpa-psk "********"
```
<sup>`!`<i> Replace `********` with WIFI username and password. Both the username and password must be set in quotes.</i></sup>   

---
<sup><i> [More Advanced Wireless Configurations...](http://github.com/rixwoodling/linux/tmp/advanced-networks.md)</i></sup>   

---
`C`  
`1` Multiple WI-FI Networks
```
apt-get install -y ip ping iw wpa_supplicant
```
<sup>```?```<i> These packages will communicate with the wifi dongle.</i></sup>    
<sup>```?```<i> [More wireless networking tools...](http://github.com/rixwoodling/linux/tmp/)</i></sup>    

><sub><i>This is a near-minimal network configuration that adds to the default network configuration. This configuration adds wireless, namely `wlan0`, still relying on a dynamic ip. The local wifi username/password is also set here. Lastly, hot-plugging is optionally added for convenience.</i></sub>  

```
# /etc/network/interfaces

auto lo
iface lo inet loopback

allow-hotplug eth0
iface eth0 inet dhcp

allow-hotplug wlan0
iface wlan0 inet manual
wpa-roam /etc/wpa_supplicant/wpa-roam.conf
  iface home inet dhcp
  iface default inet dhcp
```
```
# /etc/wpa_supplicant/wpa-roam.conf

update_config=1
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev

network={
        ssid="********"
        psk="********"
        id_str="home"
}
```
<sup>```!```<i> Both the username and password must be set in quotes.</i></sup>   

---
######Reference Links
<sup>```?```<i> https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md</i></sup>  
<sup>```?```<i> http://www.cyberciti.biz/faq/debian-linux-wpa-wpa2-wireless-wifi-networking/</i></sup>  
<sup>```?```<i> https://www.raspberrypi.org/forums/viewtopic.php?f=28&t=44044</i></sup>  
<sup>```?```<i> https://pthree.org/2012/02/26/setup-network-interfaces-in-debian/</i></sup>  
<sup>```?```<i> https://wiki.archlinux.org/index.php/Dhcpcd</i></sup>  
<sup>```?```<i> http://www.slackbook.org/html/basic-network-commands-traceroute.html</i></sup>  
<sup>```?```<i> http://www.howtogeek.com/108511/how-to-work-with-the-network-from-the-linux-terminal-11-commands-you-need-to-know/</i></sup>  
<sup>```?```<i> http://www.cyberciti.biz/faq/linux-list-network-interfaces-names-command/</i></sup>  
<sup>```?```<i> https://tty1.net/blog/2010/ifconfig-ip-comparison_en.html</i></sup>  
<sup>```?```<i> http://manpages.ubuntu.com/manpages/hardy/man5/interfaces.5.html</i></sup>  
<sup>```?```<i> http://elinux.org/RPi_Setting_up_a_static_IP_in_Debian</i></sup>  
<sup>```?```<i> http://www.tecmint.com/ip-command-examples/</i></sup>  
<sup>```?```<i> http://www.linuxjournal.com/content/wi-fi-command-line</i></sup>  
<sup>```?```<i> http://www.cyberciti.biz/faq/find-out-if-package-is-installed-in-linux/</i></sup>  
<sup>```?```<i> http://crunchbang.org/forums/viewtopic.php?id=16624</i></sup>  
<sup>```?```<i> http://raspberrypihq.com/how-to-add-wifi-to-the-raspberry-pi/</i></sup>  
<sup>```?```<i> http://raspberrypi.stackexchange.com/questions/14213/wpa-gui-cant-load-wpa-supplicant-sudo-ifup-wlan0-ignoring-unknown-interface</i></sup>  
<sup>```?```<i> https://wiki.archlinux.org/index.php/WPA_supplicant</i></sup>  
<sup>```?```<i> https://www.raspberrypi.org/forums/viewtopic.php?p=364295</i></sup>  
<sup>```?```<i> https://www.topbug.net/blog/2015/01/13/control-the-led-on-a-usb-wifi-adapter-on-linux/</i></sup>  