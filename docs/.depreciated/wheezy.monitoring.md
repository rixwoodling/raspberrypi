`WHEEZY DEPRECIATED`

##### MONITORING TEMPERATURE AND VOLTAGE

To view the Pi's temperature, type: cat /sys/class/thermal/thermal_zone0/temp. Divide the result by 1000 to get the value in Celsius.

To view the Pi's current frequency, type: cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq. Divide the result by 1000 to get the value in MHz.

To monitor the Pi's PSU voltage, you'll need a multimeter to measure between the TP1 and TP2 power supply test points; more information is available in power.

It's generally a good idea to keep the core temperature below 70 degrees and the voltage above 4.8V. Note that some USB power supplies fall as low as 4.2V; this is because they are usually designed to charge a 3.7V LiPo battery, rather than to supply 5V to a computer. If your overclocked Raspberry Pi is getting hot a heatsink can be helpful, especially if the Pi is to be run inside a case. A suitable heatsink is the self-adhesive BGA (ball-grid-array) 14x14x10 mm heatsink, available from RS Components.

##### OVERCLOCKING PROBLEMS

Most overclocking issues show up immediately with a failure to boot. If this occurs, hold down the shift key during the next boot which will temporarily disable all overclocking; this will allow you to boot successfully and then edit your settings.
