`WHEEZY DEPRECIATED`

#### Cloning `/usr/local` with git

<i>In this step, a git repository containing custom root-level files are cloned to `/usr/local` where those config files are then symbolically linked to the root `/` locations.</i>  
<sup>```?```<i> http://unix.stackexchange.com/questions/84175/create-a-symbolic-link-relative-to-the-current-directory</i></sup>  
<sup>```?```<i> http://stackoverflow.com/questions/651038/how-do-you-clone-a-git-repository-into-a-specific-folder</i></sup>  

---
###### Cloning from Github
<sup>`1` Clear out the `/usr/local` folder. `rm -r /usr/local/*`</sup>   
<sup>`2` Clone folder hierarchy into empty /usr/local folder. `git clone https://github.com/rixwoodling/linux.git /usr/local/.`</sup>   

---
###### Create Backups of Default Configurations   
```
#!/bin/bash
# This bash script creates backups of the original configuration files 

echo "Creating backups of original configuration files into .bak"

# /etc
cp -v /etc/environment /etc/environment.bak                                             
cp -v /etc/hostname /etc/hostname.bak
cp -v /etc/motd /etc/motd.bak
cp -v /etc/apt/sources.list /etc/apt/sources.list.bak
cp -v /etc/default/locale /etc/default/locale.bak
cp -v /etc/network/interfaces /etc/network/interfaces.bak
cp -v /etc/ssh/sshd_config /etc/ssh/sshd_config.bak

echo "To find all backups use: find / -name *.bak"
```

---
###### Making Root Connections
<sup>`1` Create a bash script. (If creating from scratch) `nano /usr/local/bin/connections`</sup>   
<sup>`2` Copy and paste the following bash script into <i>'connections'</i>.</sup>  

```
#!/bin/bash
# This bash script connects all root level configurations 

echo "Connecting /usr/local folders and files to root level configurations..."

# /etc
rm -rv /etc/environment                                             
rm -rv /etc/hostname
rm -rv /etc/motd
rm -rv /etc/apt/sources.list
rm -rv /etc/default/locale
rm -rv /etc/network/interfaces
rm -rv /etc/ssh/sshd_config
rm -rv /etc/skel
ln -sv /usr/local/etc/environment /etc/environment
ln -sv /usr/local/etc/hostname /etc/hostname
ln -sv /usr/local/etc/motd /etc/motd
ln -sv /usr/local/etc/apt/sources.list /etc/apt/sources.list
ln -sv /usr/local/etc/default/locale /etc/default/locale
ln -sv /usr/local/etc/network/interfaces /etc/network/interfaces
ln -sv /usr/local/etc/ssh/sshd_config /etc/ssh/sshd_config
ln -sv /usr/local/etc/skel /etc/skel

# /root
rm -rv /root
ln -sv /usr/local/root /root

# /share
rm -rv /usr/share/man/mann
ln -sv /usr/local/share/man/mann /usr/share/man/mann

```
<sup><i>Save and exit out of nano. Ensure the current directory is `cd /usr/local/bin`.</i></sup>  
<sup>`3` Change file permissions with `chmod 700 connections`.</sup>  
<sup><i>With permissions set to 700, only the root user can read, write, and execute this script.</i></sup>  

---
###### Verify Content   
```
#!/bin/bash
# This bash script creates backups of the original configuration files 

echo "Displaying content of /usr/local configuration files. Please review..."

cat \
/etc/environment \
/etc/hostname \
/etc/motd \
/etc/apt/sources.list \
/etc/default/locale \
/etc/network/interfaces \
/etc/ssh/sshd_config \
/etc/skel \
| less

```
