`WHEEZY DEPRECIATED`

##### System Management

---
##### [installation history](http://serverfault.com/questions/175504/how-do-i-get-the-history-of-apt-get-install-on-ubuntu)  
<sup><i>manually installed packages </i> `dpkg --get-selections | sed -n 's/\t\+install$//p'`</sup>  
<sup><i>automatically installed packages </i>`</var/lib/apt/extended_states awk -v RS= '/\nAuto-Installed: *1/{print$2}'`</sup>  
<sup><i>manually and automatically installed packages </i>`comm -23 <(dpkg --get-selections | sed -n 's/\t\+install$//p') <(</var/lib/apt/extended_states awk -v RS= '/\nAuto-Installed: *1/{print$2}' |sort)`</sup>  

space allocation `du -sh /* 2>/dev/null`  

#####space allocation  
#####Admin Tools  
<i>This  </i>
>`du -sch /* 2>/dev/null | sort -h -r` <i> The `du` command.   
>`du -hd1 --exclude=./proc | sort -h -r`
http://www.golinuxhub.com/2014/09/how-to-exclude-multiple-directories.html

</sup>

find  
`find / -name *.log`  
`find / -type f -name *.log`  
`find / -name "init.d"`  
`find / -iname "*.bak" -mtime -1` find all .bak files modified since a day ago.

ls  
`ls -alhF --color`  

aliases  
`alias rm='rm -i'`

set hostname  
`nano /etc/hostname`  
`nano /etc/hosts` change old name to new name...  
`reboot`    
set timezone  
`dpkg-reconfigure tzdata`  


login information  
`w`  
`who -a`  
`users`  
`last` /var/logs/wtmp  
  
`last root | grep "Sun May 17"`  

`cd /var/log/`

grep logs  
<sup>
`grep -i --text "Invalid user" /var/log/auth.log --color` [✎](http://stackoverflow.com/questions/9988379/how-to-grep-a-text-file-which-contains-some-binary-data)  
`history | grep -i "invalid"` [?](http://javarevisited.blogspot.tw/2011/03/unix-command-tutorial-working-fast-in.html) 
</sup>

find all logs  
<sup>
`find / -name "*.log"` [?](http://stackoverflow.com/questions/9988379/how-to-grep-a-text-file-which-contains-some-binary-data)  
</sup>


systemd logging info https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs  
try `journalctl |grep sshd`

---
manually rotate dpkg.log  
`cp /var/log/dpkg.log /var/log/dpkg.log.1` first create a rotate backup  
`nano /var/log/dpkg.log` delete all before a certain date creating a user-installed log.  

installed package list
`grep " install " /var/log/dpkg.log`

configure prompt to show: `root❤linux:~#`
- set locales for UT8 (otherwise the heart '❤' won't show)
- edit /etc/bash.bashrc and change `@` to `❤`.
- 
