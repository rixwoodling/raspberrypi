#### boot process
---
![Alt text](https://images.pexels.com/photos/1194713/pexels-photo-1194713.jpeg "a title")
---
###### `BIOS` > `MBR | EFI/UEFI` > `GRUB` > `kernel` > `initramfs` > `/sbin/init | systemd`




##### BIOS
---
To prevent others from changing the boot order to a Live CD for example, a password for the BIOS is good first step toward protecting your system at boot time. Access the BIOS menu at boot to set a password.



##### MBR EFI/UEFI
---
` `

##### GRUB
---
`update-grub`
`grub2-mkconfig`

##### kernel
---
`cd /boot/`

##### initramfs
---
`cd etc/initramfs-tools/`

##### sbin/init -> systemd
---
`cd /etc/init.d/`


