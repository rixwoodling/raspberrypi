openbox

git pull dotfiles to home directory
```
cd ~ && \
rm .bash_logout .bashrc .profile && \
git init && \
git remote add origin https://path/to/dotfiles.git && \
git pull origin master
```

install openbox
```
sudo apt install xorg openbox
```

install conky
```
sudo apt install conky
```
if `VCHI initialization failed` prints in HUD,<br>
add user to video group and reboot.<br>
see vcgenmd.txt for more info...
```
sudo usermod -aG video $USER
sudo reboot
```

### install tmux
```
sudo apt install tmux
```

add to .xinitrc on login
or reload .Xresources after changes if logged in.
perhaps interchangable, .Xdefaults now depreciated.
```
xrdb ~/.Xresources
```
with fonts in ~/.fonts, subfolders ok,
view installed fonts:
```
fc-list | cut -f2 -d: | sort -u
```
here are some fonts that work well in .Xresources
```
echo "xterm*faceName: SFMono:style=bold:pixelsize=12" >> ~/.Xresources
```

### rounded Corners
---
find openbox version number
```
sudo apt policy openbox
```
download necessary files from archive
if openbox is version 3.6.1+rpi2
from http://archive.raspberrypi.org/debian/pool/ui/o/openbox/
openbox_3.6.1.orig.tar.gz
openbox_3.6.1-8+rpi2.debian.tar.xz
openbox_3.6.1-8+rpi2.dsc
# download dpkg-source
```
sudo apt install dpkg-dev && \
sudo apt install pkg-config # 'could not be found or too old' during ./configure && \
sudo apt install libglib2.0-dev # 'glib-2.0' not found during ./configure
```

