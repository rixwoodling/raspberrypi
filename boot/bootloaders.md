### bootloaders

#### create disk label


```
sudo sfdisk --delete /dev/sdb && \
echo 'type=e, bootable' | sudo sfdisk /dev/sdb 1 && \
sudo mkfs.vfat /dev/sdb1 && \
sudo sfdisk -l /dev/sdb
```

```
sudo mount /dev/sdb1 /mnt && \
sudo mkdir /mnt/boot
```

install GRUB
```
sudo grub-install --target=i386-pc --boot-directory=/mnt/boot /dev/sdb
```

