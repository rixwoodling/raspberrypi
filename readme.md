<sup><i>This checklist and the instructions located in the docs folder are steps to install Linux on to the Raspberry Pi.</i></sup>   
<sup><i>These instructions use Minibian, an SD card, and have successfully been tested on a Raspberry Pi 1 Model B.</i></sup>  

---
##### Inventory List
<sup>`+` [Minibian 2016-03-12-jessie-minibian.tar.gz](https://minibianpi.wordpress.com/download/)</sup>  
<sup>`+` [SD Card](https://elinux.org/RPi_SD_cards)</sup>  
<sup>`+` [Raspberry Pi 1 Model B](https://www.adafruit.com/product/998)</sup>  

---
##### Installation and Set-Up
<sup>`+` Install Linux on an SD card with a host system. [Mac](./docs/sdcard.setup.mac.md)</sup>  
<sup>`+` [SSH](./docs/ssh.md) into the Raspberry Pi. `ssh root@192.168.X.XXX`.</sup>  
<sup>`+` [Expand](./docs/resize.md) main partition with `fdisk`.</sup>   
<sup>`+` Update and upgrade. `apt-get update -y && apt-get upgrade -y && apt-get clean`</sup>  

---
#### root Configurations  
[rotate](./docs/rotatelogs.md) dpkg and history logs.
```
cp /var/log/dpkg.log /var/log/dpkg.log.bak && \
cp /var/log/apt/history.log /var/log/apt/history.log.bak && \
truncate -s 0 /var/log/dpkg.log && \
truncate -s 0 /var/log/apt/history.log
```

install `nano` 
```
apt-get install -y nano
```
<sup>`+` Set [locale](./docs/locale.md).</sup>  
<sup>`+` Set [localtime](./docs/localtime.md).</sup>  
<sup>`+` Configure the wireless [network](./lab/%23network.staticip.txt). `nano /etc/network/interfaces`</sup>  
<sup>`+` Update and upgrade. `apt-get update -y && apt-get upgrade -y && apt-get clean`</sup>    

##### Admin Essentials  
<sup>`+` Set up a strong root [password](password.md). `passwd`</sup>   
<sup>`+` Add an admin user. `adduser user` <i>(Creates user and group by default.)</i></sup>  
<sup>`+` Install [sudo](sudo.md). `apt-get install sudo`</sup>  
<sup>`+` Give admin user `sudo` permission via [visudo](/tmp/sudo.md). `visudo`</sup>  
<sup>`+` Disable ssh into root. `nano /etc/ssh/sshd_config`</i></sup>  

##### Server Setup  
<sup>`+` Mount ext4 external hard drive to RPi [password](mount.md). `mount`</sup>   
<sup>`+` Add an admin user. `adduser user` <i>(Creates user and group by default.)</i></sup>  
