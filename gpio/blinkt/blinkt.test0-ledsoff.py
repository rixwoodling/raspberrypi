from blinkt import set_pixel, clear, show

for i in range( 8 ) :
    clear()
    set_pixel( i,0,0,0 )
    show()
