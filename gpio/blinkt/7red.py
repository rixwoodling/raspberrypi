import time
from blinkt import set_pixel, set_brightness, show, clear

clear()
set_brightness( 0.1 )

while True :
    clear()
    set_pixel( 7,100,0,0 )
    show()

    time.sleep(30)

    clear()
    set_pixel( 7,0,0,0 )
    show()

    time.sleep(30)
