#!/bin/bash
# blinkt! VPN script

# if VPN is off, red led #7
# if VPN is on, green led #7
# all other issues, yellow led #7

if [[ `expressvpn status` == "Not connected" ]]; then
    echo $blinkt/7red.py

elif [[ `expressvpn status` =~ "Connected" ]]; then
    python ../7green.py
else
    python ../7blue.py
fi
