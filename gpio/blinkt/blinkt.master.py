#!/usr/bin/python
# -*- coding: utf-8 -*-

# ---

import subprocess
from blinkt import clear, show, set_pixel
import time

# ---

def green() :
    #while True :
        clear(); set_pixel( 7,0,100,0 ); show(); time.sleep( 5 )

def red() :
    #while True :
        clear(); set_pixel( 7,100,0,0 ); show(); time.sleep( 5 )

def problem() :
    #while True :
        clear(); set_pixel( 7,100,0,0 ); show()
        time.sleep( 1 )
        clear(); set_pixel( 7,0,0,0 ); show()
        time.sleep( 1 )

# ---

def vpn():
    vpn = "expressvpn status | head -n1 | awk '{print $1}' | sed 's,\x1B\[[0-9;]*m,,g'"
    output = subprocess.check_output( vpn,shell=True )

    if output.strip() == 'Not' :
        red()
    elif output.strip() == 'Connected' :
        green()
    else :
        problem()

# ---

while True :
    vpn()
    #print( '1' ) #debug
