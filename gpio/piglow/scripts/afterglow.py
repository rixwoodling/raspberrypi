#!/usr/bin/env python
# -*- coding: utf-8 -*-

# afterglow

from piglow import PiGlow
from time import sleep
from random import choice

def afterglow():
  for i in range(2):
    piglow = PiGlow()
    piglow.all(0)
    a = 1
    while a <= 18:
      piglow.led(a, 100)
      print(a, 100)
      sleep(0.1)
      piglow.led(a, 0)
      print(a, 0)

      a = a + 1
    piglow.all(0)
afterglow()