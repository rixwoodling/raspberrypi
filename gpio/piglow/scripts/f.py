# coding: utf-8
# python 2.7
# piglow /// fill all leds forward then reverse 

from piglow import PiGlow
from time import sleep
from random import choice

def f():
        piglow = PiGlow()
        a = 1
        b = 1
        c = 0
        d = 50
        e = 0.1

        piglow.all(0)
        for x in range(2):
           for y in range(3):
              a = ((a * b) + c)
              for z in range(6):
                piglow.led(a,d)
                sleep(e)
                a = a + b
              c = c + (6 * b)
              a = 1
           b = -1
           c = 19
           d = 0
f()