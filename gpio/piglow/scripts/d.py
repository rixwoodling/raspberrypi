# coding: utf-8
# python 2.7
# piglow /// spread and rotate in unison

from piglow import PiGlow
from time import sleep
from random import choice

def d():
   for i in range(4):
        piglow = PiGlow()
        piglow.all(0)
        d = 1
        while d <= 6:
          piglow.colour(d, 100)
          sleep(0.1)
          piglow.colour(d, 0)
          d = d + 1
d()