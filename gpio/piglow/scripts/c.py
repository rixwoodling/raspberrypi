# coding: utf-8
from piglow import PiGlow
from time import sleep
from random import choice

def c():
        for i in range(20):
                piglow = PiGlow()
                piglow.all(0)
                a = 100
                b = 0
                while a > 0:
                        # blue > purple > red
                        piglow.colour("blue", a)
                        piglow.colour("red", b)
                        sleep(0.1)
                        a = a - 10
                        b = b + 10
                a = 100
                b = 0
                while a > 0:
                        # red > orange > yellow
                        piglow.colour("red", a)
                        piglow.colour("yellow", b)
                        sleep(0.1)
                        a = a - 10
                        b = b + 10
                a = 100
                b = 0
                while a > 0:
                        # yellow > green > blue
                        piglow.colour("yellow", a)
                        piglow.colour("blue", b)
                        sleep(0.1)
                        a = a - 10
                        b = b + 10
        piglow.all(0)
c()