# coding: utf-8

from piglow import PiGlow
from time import sleep
from random import choice

def b():
        for i in range(4):
                piglow = PiGlow()
                piglow.all(0)
                count = 0
                a = 1
                b = 7
                c = 13
                while count <= 5:
                        piglow.led(a, 100)
                        piglow.led(b, 100)
                        piglow.led(c, 100)
                        sleep(0.1)
                        piglow.led(a, 0)
                        piglow.led(b, 0)
                        piglow.led(c, 0)
                        a = a + 1
                        b = b + 1
                        c = c + 1
                        piglow.all(0)
                        count = count + 1
b()