#!/usr/bin/env python
# -*- coding: utf-8 -*-

from random import shuffle
from afterglow import afterglow
from b import b
from d import d
from e import e
from f import f
from g import g
from h import h
from j import j
from n import n

a = afterglow()
b = b()
d = d()
e = e()
f = f()
g = g()
h = h()
j = j()
n = n()

sequence = [a,b,d,e,f,g,h,j,n]

for i in range(2):
  shuffle(sequence)
