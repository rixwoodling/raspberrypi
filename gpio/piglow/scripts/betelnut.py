# coding: utf-8

from piglow import PiGlow
from time import sleep
from random import choice

def A():
	for i in range(2):
		piglow = PiGlow()
		piglow.all(0)
		a = 1
		while a <= 18:
			piglow.led(a, 100)
			sleep(0.5)
			piglow.led(a, 0)
			a = a + 1
		piglow.update_leds()

def B():
	for i in range(2):
		piglow = PiGlow()
		piglow.all(0)
		count = 0
		a = 1
		b = 7
		c = 13
		while count <= 6:
			pyglow.led(a, 100)
			pyglow.led(b, 100)
			pyglow.led(c, 100)
			sleep(0.1)
			pyglow.led(a, 0)
			pyglow.led(b, 0)
			pyglow.led(c, 0)
			a = a + 1
			b = b + 1
			c = c + 1
			
			piglow.update_leds()
			count = count + 1

def E():
	for i in range(2):
		piglow = PiGlow()
		piglow.all(0)
		a = 100
		b = 0
		while a > 0:
			# blue > purple > red
			pyglow.color("blue", a)
			pyglow.color("red", b)
			sleep(0.1)
			a = a - 10
			b = b + 10
		piglow.update_leds()	
		a = 100
		b = 0
		while a > 0:
			# red > orange > yellow
			pyglow.color("red", a)
			pyglow.color("yellow", b)
			sleep(0.1)
			a = a - 10
			b = b + 10
		piglow.update_leds()
		a = 100
		b = 0
		while a > 0:
			# yellow > green > blue
			pyglow.color("yellow", a)
			pyglow.color("blue", b)
			sleep(0.1)
			a = a - 10
			b = b + 10
		piglow.update_leds()


choice([A],[B],[C])()
