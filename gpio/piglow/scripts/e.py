# coding: utf-8
# python 2.7
# piglow /// stack leds one at a time

from piglow import PiGlow
from time import sleep
from random import choice

def e():
        piglow = PiGlow()
        piglow.all(0)

        a = 1
        b = 0
        c = 5
        d = 0
        e = 50

        for x in range(3):
           for y in range(6):
              for z in range(c):
                piglow.led(a, e)
                sleep(0.1)
                piglow.led(a, 0)
                print(a),
                a = a + 1
              piglow.led(a, e)
              print(a)
              a = 1 + d
              c = c - 1
           piglow.led(a, e)
           print(a)
           b = b + 1
           d = (6 * b)
           a = d + 1
           c = 5

        a = 1
        for w in range(18):
           sleep(0.1)
           piglow.led(a, 0)
           a = a + 1
e()
