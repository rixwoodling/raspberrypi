##### install piglow notes

`1` install python packages  
>`$` `sudo apt-get update`  
>`$` `sudo apt-get install python-smbus python-psutil -y`  

`2` append to modules `sudo nano /etc/modules`  
```
i2c-dev
i2c-bcm2708
```

`3` comment out (if exists) `sudo nano /etc/modprobe.d/raspi-blacklist.conf`  
```
# blacklist spi-bcm2708
# blacklist i2c-bcm2708
```

add i2c to boot config `sudo nano /boot/config.txt`
```
dtparam=i2c_arm=on
```

reboot
$   sudo reboot

create piglow folder in root  
$   sudo mkdir /root/piglow  
$   cd /root/piglow  

download necessary files  
$   wget https://raw.github.com/Boeeerb/PiGlow/master/piglow.py  
$   wget https://raw.github.com/Boeeerb/PiGlow/master/Examples/test.py  


$   sudo python test.py

#
https://github.com/Boeeerb/PiGlow/blob/master/Examples/

#
from piglow import PiGlow

#
piglow = PiGlow()

#
$   crontab -e
    ---
+   @reboot python /root/piglow/master.py &
+   @hourly python /root/piglow/master.py && rm /root/piglow/*.pyc

#
tail grep cron /var/log/syslog


troubleshooting

!   IOError: [Errno 2] No such file or directory?
>   update kernel, possibly out of date, wheezy not supported


links

https://github.com/Boeeerb/PiGlow
http://docs.pimoroni.com/piglow/#


<i>Running `python piglowtest.py` without being root will cause this error:</i>   
```
Traceback (most recent call last):
  File "piglowtest.py", line 4, in <module>
    pyglow = PyGlow()
  File "/home/rix/piglow/PyGlow.py", line 117, in __init__
    self.bus = SMBus(i2c_bus)
IOError: [Errno 13] Permission denied
```
<i>Instead, use `sudo python piglowtest.py`</i>  

