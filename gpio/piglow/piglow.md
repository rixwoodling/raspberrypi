```
         07
       08
      09
      06     0C 0E
       05  0B      10 
         0A   0D      11
              0F       12
  01        04
      02 03
      
```

#### piglow

<i>"The PiGlow is a small add on board for the Raspberry Pi that provides 18 individually controllable LEDs."</i>

---
#### install package library and python packages 

`python3` on debian bullseye
```
sudo apt install python3-piglow
```
```
sudo apt install python3-smbus python3-rpi.gpio python3-psutil
```
<br>

`pip2` on bullseye  
```
sudo apt install curl
curl https://bootstrap.pypa.io/pip/2.7/get-pip.py --output get-pip.py
sudo python2 get-pip.py

pip2 --version
```
```
sudo pip2 install piglow
```
```
sudo pip2 install python-smbus python-psutil
```
><i>`python2` libraries are transitioning away from development and from being included in repository sources. Installing `python-piglow` from `apt` still works up to debian bullseye release, but some other required packages, such as `python-smbus` and `python-psutil`, are no longer included in bullseye sources and must be installed via `pip2`. however, `python-pip` is also removed from bullseye sources, forcing most python2 installation to happen through pip2.</i>
<br>

`python2` on buster
```
sudo apt install python-piglow python-smbus python-rpi.gpio python-psutil
```
><i>up until buster, apt sources will install all packages required for python2. however, most of these packages will be removed after upgrading to bullseye.</i>
<br>

---
#### config ( raspberry pi )

<i>append kernel arguments to `/etc/modules` if missing;</i> 
```
sudo echo 'i2c-dev' >> /etc/modules && \
sudo echo 'i2c-bcm2708' >> /etc/modules
```
<i>ensure the following from `raspi-blacklist.conf` is commented out if the file exists;</i>
```
sudo nano /etc/modprobe.d/raspi-blacklist.conf
```
>`# blacklist spi-bcm2708`  
>`# blacklist i2c-bcm2708`  

<i>add i2c argument to boot config;</i>
```
sudo echo 'dtparam=i2c_arm=on' >> /boot/config.txt
```
<i>add user to i2c group; then</i>
```
sudo adduser $USER i2c
```
<i>reboot</i>
```
sudo reboot
```
<br>

---
#### getting started

`python3`
```
sudo git clone https://github.com/pimoroni/piglow.git /usr/local/piglow && \
cd /usr/local/piglow && \
sudo python3 examples/bar.py
```
<br>

`python2`

download necessary files using either `git` or `wget`
```
sudo git clone https://github.com/Boeeerb/PiGlow.git /usr/local/piglow
cd /usr/local/piglow && \
```
```
sudo mkdir /usr/local/piglow && \
cd /usr/local/piglow && \
sudo wget https://raw.github.com/Boeeerb/PiGlow/master/piglow.py && \
sudo wget https://raw.github.com/Boeeerb/PiGlow/master/Examples/test.py && \
sudo python2 test.py
```

---
#### scripts

```
# 
sudo python test.py

#
https://github.com/Boeeerb/PiGlow/blob/master/Examples/

#
from piglow import PiGlow

#
piglow = PiGlow()

# add script to startup and hourly times
crontab -e
 ---
 @reboot python /path/to/script.py
 @hourly python /path/to/script.py

# restart cron service
sudo service cron restart
sudo service cron status

# logs for cron ( if needed )
sudo tail grep cron /var/log/syslog
```

#### troubleshooting
```
IOError: [Errno 2] No such file or directory?
# update kernel, possibly out of date, wheezy not supported
```
```
    self.bus = SMBus(i2c_bus)
IOError: [Errno 13] Permission denied
# traceback error without being root will cause this error. Either use `sudo` or add user to i2c group.
```
```
ImportError: No module named piglow
# this is caused by python2 trying to run a script calling for a module either not in built-in module list 
# or piglow.py module is not in same directory.
```
---

#### links
https://github.com/Boeeerb/PiGlow  
http://docs.pimoroni.com/piglow/#

