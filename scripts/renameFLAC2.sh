#!/bin/bash

# script converts every space ' ' to a dot .
# script converts every comma , to a dot .
# script converts every double-dot .. to a single dot .

for a in *.flac; do b=${a// /.}; c="${b//,/.}"; d="${c//../.}"; \
mv "$a" "$d"; done
