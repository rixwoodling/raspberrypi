#!/bin/bash
# file format: 01 TITLE.flac

# 1. Rip from CD from iTunes to .aif
# 2. Copy this script to folder with .aif files
# 3. Copy album cover to folder and rename to 'folder.png'
# 3. chmod 777 *.sh
# 4. ./aif2flac.sh 

flac *.aif
rm *.aif

# remove all metadata and verify
#metaflac --preserve-modtime --remove-all-tags
# debug
#metaflac --list *.flac | grep comment

read -p 'Enter Artist: ' artist
read -p 'Enter Album Artist: ' albumartist
read -p 'Enter Album: ' album
read -p 'Enter Genre: ' genre
read -p 'Enter Year: ' year

# 1

# PADDING
# Padding is the added filesize when adding metadata or an album cover.
# If adding a 2MB to a 10MB flac, the size will become 12MB. But deleting the
# image and readding another image will increase the size to 14MB. Resetting the
# padding with --dont-use-padding will remove the 4MBs plus erasing all other
# metadata! There are ways to reserve padding space other than redoing metadata.
#
# view current padding size in bytes
metaflac --preserve-modtime --list --block-type=PADDING *.flac
# remove all padding, equivalent to resetting all metadata to nothing. 
metaflac --preserve-modtime --dont-use-padding --remove \
--block-type=PADDING *.flac

# COVERART
# first remove picture, then add picture (located in same folder as flac)
metaflac --preserve-modtime --remove --block-type=PICTURE \
--dont-use-padding *.flac
metaflac --preserve-modtime --import-picture-from=folder.png *.flac

# 2

# DISCNUMBER
# debug:
# for i in *.flac; do echo $(echo "$i" | cut -d ' ' -f 1 | cut -sd '-' -f 1); done
# 
# remove, set, verify
metaflac --preserve-modtime --remove-tag=DISCNUMBER *.flac
for i in *.flac; do metaflac --preserve-modtime \
--set-tag=DISCNUMBER=$(echo "$i" | cut -d ' ' -f 1 | cut -d '-' -f 1) "$i"; done
metaflac --list *.flac | grep -F "DISCNUMBER"

# TRACKNUMBER
# debug:
# for i in *.flac; do echo $(echo "$i" | cut -d ' ' -f 1 | cut -d '-' -f 2); done
#
# remove, set, verify
metaflac --preserve-modtime --remove-tag=TRACKNUMBER *.flac
for i in *.flac; do metaflac --preserve-modtime \
--set-tag=TRACKNUMBER=$(echo "$i" | cut -d ' ' -f 1 | cut -d '-' -f 2) "$i"; done
metaflac --list *.flac | grep -F "TRACKNUMBER"

# TITLE
# debug:
# for i in *.flac; do echo $(echo "${i%.*}" | cut -d ' ' -f2- | cut -f 1 -d '.'); done
#
# remove, set, verify
metaflac --preserve-modtime --remove-tag=TITLE *.flac
for i in *.flac; do TITLE=$(echo "${i%.*}" | cut -d ' ' -f2-); \
metaflac --preserve-modtime --set-tag=TITLE="${TITLE}" "$i"; done
metaflac --list *.flac | grep -F "TITLE"

# ARTIST
# remove, set, verify
metaflac --preserve-modtime --remove-tag=ARTIST *.flac
metaflac --preserve-modtime --set-tag=ARTIST="${artist}" *.flac
metaflac --list *.flac | grep -F "ARTIST" | grep -Fv "ALBUM"

# ALBUM ARTIST
# remove, set, verify
metaflac --preserve-modtime --remove-tag=ALBUM\ ARTIST *.flac
metaflac --preserve-modtime --set-tag=ALBUM\ ARTIST="${albumartist}" *.flac
metaflac --list *.flac | grep -F "ALBUM ARTIST"

# ALBUM
# remove, set, verify
metaflac --preserve-modtime --remove-tag=ALBUM *.flac
metaflac --preserve-modtime --set-tag=ALBUM="${album}" *.flac
metaflac --list *.flac | grep -F "ALBUM"

# YEAR
# remove, set, verify
metaflac --preserve-modtime --remove-tag=DATE *.flac
metaflac --preserve-modtime --set-tag=DATE="${year}" *.flac
metaflac --list *.flac | grep -F "DATE="

# GENRE
# remove, set, verify
metaflac --preserve-modtime --remove-tag=GENRE *.flac
metaflac --preserve-modtime --set-tag=GENRE="${genre}" *.flac
metaflac --list *.flac | grep -F "GENRE"

# REPLAYGAIN
metaflac --remove-replay-gain *.flac
metaflac --add-replay-gain *.flac
metaflac --list *.flac | grep -F "REPLAYGAIN_TRACK_GAIN"

# 3

# RENAME FLAC
for a in *.flac; do b=${a// /.}; c="${b//,/.}"; d="${c//../.}"; \
mv "$a" "$d"; done

