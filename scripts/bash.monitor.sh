#!/bin/bash
# -*- coding: utf-8 -*-

space=" "
cpu=`/opt/vc/bin/vcgencmd measure_temp`
ram=""
volts=`vcgencmd measure_volts core` | sed 's/\(\.[0-9][0-9]\)[0-9]*/\1/g'
externalip=`wget -qO- http://ipecho.net/plain | xargs echo`

cpuram=`printf "%s %s %s\n" $cpu $space $ram $space`
cpuramvolts=`printf "%s %s %s\n" $volts $externalip`

system=`uname -a`
monitor=`printf "%s %s %s\n" $cpuram $space $volts`

echo $system
echo $monitor

