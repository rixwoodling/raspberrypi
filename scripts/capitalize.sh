#!/usr/bin/env python
# -*- coding: utf-8 -*-

echo Capitalize all words.

for i in *; do \
awk '{for(i=1;i<=NF;i++){ $i=toupper(substr($i,1,1)) substr($i,2) }}1' $i;
done

echo done

