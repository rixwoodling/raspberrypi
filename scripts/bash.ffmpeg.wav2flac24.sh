#!/usr/bin/env python
# -*- coding: utf-8 -*-

echo "Encode 176kHz 24/32 bit WAV to FLAC"

for i in *.wav; do \
ffmpeg -i $i -af aformat=s32:176000 $i.flac; \
done

