#!/usr/bin/env python
# -*- coding: utf-8 -*-

echo Remux MKV to MP4, re-encode DTS to ACC 5.1

for i in *.mkv; do \
ffmpeg -i $i -c:v copy -c:a aac -b:a 384k $i.mp4; \
done

echo done
