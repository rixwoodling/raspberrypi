#!/bin/bash

# prompts for artist name, reformats filename to: 
# before 01 Song Title.flac
# after  01.Artist.Song.Title.flac
#
# script converts all spaces ' ' to dots .
# script converts all commas , to dots .
# script converts all double-dots .. to single dots .

read -p 'Enter Artist: ' artist; for a in *.flac; do \
title=$(echo "$a"| cut -d ' ' -f2-) track=$(echo "$a"| cut -d ' ' -f1); \
b=${track}.${artist// /.}.${title// /.}; c="${b//,/.}"; d="${c//../.}"; \
mv "$a" "$d"; done

