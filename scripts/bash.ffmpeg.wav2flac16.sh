#!/usr/bin/env python
# -*- coding: utf-8 -*-

echo "Encode 44.1kHz 16 bit WAV to FLAC"

for i in *.wav; do \
ffmpeg -i $i -af aformat=s16:44100 $i.flac; \
done

