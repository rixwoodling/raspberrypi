#!/usr/bin/env python
# -*- coding: utf-8 -*-

echo "Embed English SRT subtitles to MP4"

for i in *.mp4; do \
ffmpeg -i $i -i *.srt \
-c:s mov_text -c:v copy -c:a copy \
-map 0:v -map 0:a -map 1 \
-metadata:s:s:0 language=eng \
$i.mp4; \
done

